import time
import requests

from PageObjects.HomePage import HomePage


class AllSectionsPresentInHeader(HomePage):

    def setUp(self, **kwargs):
        super().setUp()
        self.open_page()

    # Test names should start with 'test_'
    def test_purchase_present_in_header(self):
        self.click(self.purchase_in_header)
        time.sleep(5)
        self.assertIn('https://strana.artw.dev/purchase', self.get_current_url())
        r = requests.get(self.get_current_url())
        self.assertEqual(200, r.status_code)

    def test_projects_present_in_header(self):
        self.click(self.projects_in_header)
        time.sleep(5)
        self.assertEqual(self.get_current_url(), 'https://strana.artw.dev/projects')
        r = requests.get(self.get_current_url())
        self.assertEqual(200, r.status_code)

    def test_flats_present_in_header(self):
        self.click(self.flats_in_header)
        time.sleep(10)
        self.assertEqual(self.get_current_url(), 'https://strana.artw.dev/flats')
        r = requests.get(self.get_current_url())
        self.assertEqual(200, r.status_code)

    def test_commercial_present_in_header(self):
        self.click(self.commercial_in_header)
        time.sleep(5)
        self.assertEqual(self.get_current_url(), 'https://strana.artw.dev/projects?kind=commercial')
        r = requests.get(self.get_current_url())
        self.assertEqual(200, r.status_code)

    def test_massmedia_present_in_header(self):
        self.click(self.massmedia_in_header)
        time.sleep(5)
        self.assertIn('https://strana.artw.dev/mass-media/', self.get_current_url())
        r = requests.get(self.get_current_url())
        self.assertEqual(200, r.status_code)

    def test_about_present_in_header(self):
        self.click(self.about_in_header)
        time.sleep(5)
        self.assertEqual('https://strana.artw.dev/about/', self.get_current_url())
        r = requests.get(self.get_current_url())
        self.assertEqual(200, r.status_code)

    def test_purchase_methods_present_in_header(self):
        self.click(self.purchase_methods_in_header)
        time.sleep(5)
        self.assertIn('https://strana.artw.dev/purchase-methods/', self.get_current_url())
        r = requests.get(self.get_current_url())
        self.assertEqual(200, r.status_code)

    def test_vacancy_present_in_header(self):
        self.click(self.vacancy_in_header)
        time.sleep(5)
        self.assertEqual('https://strana.artw.dev/about/vacancy', self.get_current_url())
        r = requests.get(self.get_current_url())
        self.assertEqual(200, r.status_code)

    def test_phone_number_presents_in_header(self):
        self.assert_element_present(self.phone_number_in_header)

    def test_favorites_present_in_header(self):
        self.click(self.favorites_in_header)
        time.sleep(5)
        self.assertEqual('https://strana.artw.dev/favorites/',  self.get_current_url())
        r = requests.get(self.get_current_url())
        self.assertEqual(200, r.status_code)

    def test_personal_area_presents_in_header(self):
        self.click(self.personal_area_in_header)

    def tearDown(self):
        super().tearDown()
