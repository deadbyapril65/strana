import time

from PageObjects.FlatsPage import FlatsPage


class FilterTest(FlatsPage):

    def setUp(self, **kwargs):
        super().setUp()
        self.open_page()

    def test_filter_domashniy(self):
        self.click(self.projects_menu)
        self.click(self.projects_domashniy)
        expected_project = self.get_text(self.projects_domashniy)
        time.sleep(10)
        self.open_random_flat()
        self.switch_to_newest_tab()
        actual_project = self.get_text(self.lots_project)
        self.assertIn(actual_project, expected_project)

    def test_filter_serdce_sibiri(self):
        self.click(self.projects_menu)
        self.click(self.projects_serdce_sibiri)
        expected_project = self.get_text(self.projects_serdce_sibiri)
        time.sleep(10)
        self.open_random_flat()
        self.switch_to_newest_tab()
        actual_project = self.get_text(self.lots_project)
        self.assertIn(actual_project, expected_project)

    def test_filter_zvezdny(self):
        self.click(self.projects_menu)
        self.click(self.projects_zvezdny)
        expected_project = self.get_text(self.projects_zvezdny)
        time.sleep(10)
        self.open_random_flat()
        self.switch_to_newest_tab()
        actual_project = self.get_text(self.lots_project)
        self.assertIn(actual_project, expected_project)

    def test_filter_kolumb(self):
        self.click(self.projects_menu)
        self.click(self.projects_kolumb)
        expected_project = self.get_text(self.projects_kolumb)
        time.sleep(10)
        self.open_random_flat()
        self.switch_to_newest_tab()
        actual_project = self.get_text(self.lots_project)
        self.assertIn(actual_project, expected_project)

    def test_filter_evropeiskiy_bereg(self):
        self.click(self.projects_menu)
        self.click(self.projects_evropeiskiy_bereg)
        expected_project = self.get_text(self.projects_evropeiskiy_bereg)
        time.sleep(10)
        self.open_random_flat()
        self.switch_to_newest_tab()
        actual_project = self.get_text(self.lots_project)
        self.assertIn(actual_project, expected_project)

    def test_filter_studio(self):
        self.click(self.rooms_quantity_studio)
        expected_rooms = self.get_text(self.rooms_quantity_studio)
        time.sleep(10)
        self.open_random_flat()
        self.switch_to_newest_tab()
        actual_rooms = self.get_text(self.lots_rooms_quantity)
        self.assertIn(expected_rooms, actual_rooms)

    def test_filter_one_room(self):
        self.click(self.rooms_quantity_one)
        expected_rooms = self.get_text(self.rooms_quantity_one)
        time.sleep(10)
        self.open_random_flat()
        self.switch_to_newest_tab()
        actual_rooms = self.get_text(self.lots_rooms_quantity)
        self.assertIn(expected_rooms, actual_rooms)

    def test_filter_two_rooms(self):
        self.click(self.rooms_quantity_two)
        expected_rooms = self.get_text(self.rooms_quantity_two)
        time.sleep(10)
        self.open_random_flat()
        self.switch_to_newest_tab()
        actual_rooms = self.get_text(self.lots_rooms_quantity)
        self.assertIn(expected_rooms, actual_rooms)

    def test_filter_three_rooms(self):
        self.click(self.rooms_quantity_three)
        expected_rooms = (self.get_text(self.rooms_quantity_three))
        time.sleep(10)
        self.open_random_flat()
        self.switch_to_newest_tab()
        actual_rooms = self.get_text(self.lots_rooms_quantity)
        self.assertIn(expected_rooms, actual_rooms)

    def test_filter_four_rooms(self):
        self.click(self.rooms_quantity_four_plus)
        expected_rooms = self.get_text(self.rooms_quantity_four_plus)
        expected_rooms = expected_rooms[0]
        time.sleep(10)
        self.open_random_flat()
        self.switch_to_newest_tab()
        actual_rooms = self.get_text(self.lots_rooms_quantity)
        self.assertIn(expected_rooms, actual_rooms)

    def test_filter_square_from_17_to_99(self):
        self.clear(self.square_from)
        self.send_keys(self.square_from, "17")
        self.clear(self.square_to)
        self.send_keys(self.square_to, "99")
        square_from = self.get_text(self.square_from)
        square_to = self.get_text(self.square_to)
        self.click(self.projects_menu)
        time.sleep(20)
        self.open_random_flat()
        self.switch_to_newest_tab()
        actual_square = self.get_square(self.lots_square)
        self.assertTrue(float(square_from) <= actual_square <= float(square_to))

    def test_filter_square_from_100_to_162(self):
        self.clear(self.square_from)
        self.send_keys(self.square_from, "100")
        self.clear(self.square_to)
        self.send_keys(self.square_to, "162")
        square_from = self.get_text(self.square_from)
        square_to = self.get_text(self.square_to)
        self.click(self.projects_menu)
        time.sleep(20)
        self.open_random_flat()
        self.switch_to_newest_tab()
        actual_square = self.get_square(self.lots_square)
        self.assertTrue(float(square_from) <= actual_square <= float(square_to))

    def test_filter_cost_from_2540000_to_9999999(self):
        cost_from = 2540000
        cost_to = 9999999

        self.clear(self.cost_from)
        self.send_keys(self.cost_from, str(cost_from))
        self.clear(self.cost_to)
        self.send_keys(self.cost_to, str(cost_to))
        self.click(self.projects_menu)
        time.sleep(20)
        self.open_random_flat()
        self.switch_to_newest_tab()
        actual_cost = self.get_cost_lots(self.lots_cost)
        self.assertTrue(cost_from <= actual_cost <= cost_to)

    def test_filter_cost_from_10000000_to_15890000(self):
        cost_from = 10000000
        cost_to = 15890000

        self.clear(self.cost_from)
        self.send_keys(self.cost_from, str(cost_from))
        self.clear(self.cost_to)
        self.send_keys(self.cost_to, str(cost_to))
        self.click(self.projects_menu)
        time.sleep(20)
        self.open_random_flat()
        self.switch_to_newest_tab()
        actual_cost = self.get_cost_lots(self.lots_cost)
        self.assertTrue(cost_from <= actual_cost <= cost_to)

    def test_filter_house_dom_sibirskiy(self):
        self.click(self.houses_menu)
        self.click(self.houses_dom_sibirskiy)
        expected_house = self.get_text(self.houses_dom_sibirskiy)
        time.sleep(10)
        self.open_random_flat()
        self.switch_to_newest_tab()
        actual_house = self.get_text(self.lots_house)
        self.assertIn(actual_house, expected_house)

    def test_filter_house_kvartal_gazovikov(self):
        self.click(self.houses_menu)
        self.click(self.houses_kvartal_gazovikov)
        expected_house = self.get_text(self.houses_kvartal_gazovikov)
        time.sleep(10)
        self.open_random_flat()
        self.switch_to_newest_tab()
        actual_house = self.get_text(self.lots_house)
        self.assertIn(actual_house, expected_house)

    def test_filter_house_kvartal_geologov(self):
        self.click(self.houses_menu)
        self.click(self.houses_kvartal_geologov)
        expected_house = self.get_text(self.houses_kvartal_geologov)
        time.sleep(10)
        self.open_random_flat()
        self.switch_to_newest_tab()
        actual_house = self.get_text(self.lots_house)
        self.assertIn(actual_house, expected_house)

    # def test_filter_house_kvartal_neftyanikov(self):
    #     self.click(self.houses_menu)
    #     self.click(self.houses_kvartal_neftyanikov)
    #     expected_house = self.get_text(self.houses_kvartal_neftyanikov)
    #     time.sleep(10)
    #     self.open_random_flat()
    #     self.switch_to_newest_tab()
    #     actual_house = self.get_text(self.lots_house)
    #     self.assertIn(actual_house, expected_house)

    def test_filter_house_kvartal_polyarnikov(self):
        self.click(self.houses_menu)
        self.click(self.houses_kvartal_polyarnikov)
        expected_house = self.get_text(self.houses_kvartal_polyarnikov)
        time.sleep(10)
        self.open_random_flat()
        self.switch_to_newest_tab()
        actual_house = self.get_text(self.lots_house)
        self.assertIn(actual_house, expected_house)

    def test_filter_house_dom_neptun(self):
        self.click(self.houses_menu)
        self.click(self.houses_dom_neptun)
        expected_house = self.get_text(self.houses_dom_neptun)
        time.sleep(10)
        self.open_random_flat()
        self.switch_to_newest_tab()
        actual_house = self.get_text(self.lots_house)
        self.assertIn(actual_house, expected_house)

    def test_filter_house_dom_uran(self):
        self.click(self.houses_menu)
        self.click(self.houses_dom_uran)
        expected_house = self.get_text(self.houses_dom_uran)
        time.sleep(10)
        self.open_random_flat()
        self.switch_to_newest_tab()
        actual_house = self.get_text(self.lots_house)
        self.assertIn(actual_house, expected_house)

    def test_filter_house_dom_cuba(self):
        self.click(self.houses_menu)
        self.click(self.houses_dom_cuba)
        expected_house = self.get_text(self.houses_dom_cuba)
        time.sleep(10)
        self.open_random_flat()
        self.switch_to_newest_tab()
        actual_house = self.get_text(self.lots_house)
        self.assertIn(actual_house, expected_house)

    def test_filter_house_dom_salvador(self):
        self.click(self.houses_menu)
        self.click(self.houses_dom_salvador)
        expected_house = self.get_text(self.houses_dom_salvador)
        time.sleep(10)
        self.open_random_flat()
        self.switch_to_newest_tab()
        actual_house = self.get_text(self.lots_house)
        self.assertIn(actual_house, expected_house)

    # def test_filter_house_dom_london(self):
    #     self.click(self.houses_menu)
    #     self.click(self.houses_dom_london)
    #     expected_house = self.get_text(self.houses_dom_london)
    #     time.sleep(10)
    #     self.open_random_flat()
    #     self.switch_to_newest_tab()
    #     actual_house = self.get_text(self.lots_house)
    #     self.assertIn(actual_house, expected_house)

    def test_filter_floor_from_1_to_9(self):
        floor_from = 1
        floor_to = 9

        self.clear(self.floor_from)
        self.send_keys(self.floor_from, str(floor_from))
        self.clear(self.floor_to)
        self.send_keys(self.floor_to, str(floor_to))
        self.click(self.projects_menu)
        time.sleep(10)
        self.open_random_flat()
        self.switch_to_newest_tab()
        actual_floor = int(self.get_text(self.lots_floor)[0])
        self.assertTrue(floor_from <= actual_floor <= floor_to)

    def test_filter_floor_from_10_to_25(self):
        floor_from = 10
        floor_to = 25

        self.clear(self.floor_from)
        self.send_keys(self.floor_from, str(floor_from))
        self.clear(self.floor_to)
        self.send_keys(self.floor_to, str(floor_to))
        self.click(self.projects_menu)
        time.sleep(10)
        self.open_random_flat()
        self.switch_to_newest_tab()
        actual_floor = int(self.get_text(self.lots_floor)[0:2])
        self.assertTrue(floor_from <= actual_floor <= floor_to)

    def test_filter_floor_from_10_to_10(self):
        floor_from = 10
        floor_to = 10

        self.clear(self.floor_from)
        self.send_keys(self.floor_from, str(floor_from))
        self.clear(self.floor_to)
        self.send_keys(self.floor_to, str(floor_to))
        self.click(self.projects_menu)
        time.sleep(10)
        self.open_random_flat()
        self.switch_to_newest_tab()
        actual_floor = int(self.get_text(self.lots_floor)[0:2])
        self.assertTrue(floor_from <= actual_floor <= floor_to)

    def test_filter_terrace(self):
        self.click(self.terrace)
        time.sleep(10)
        self.open_random_flat()
        self.switch_to_newest_tab()
        self.assert_element_visible(self.lots_terrace)

    def test_filter_parking_as_a_gift(self):
        self.click(self.parking_as_a_gift)
        time.sleep(10)
        self.open_random_flat()
        self.switch_to_newest_tab()
        self.assert_element_visible(self.lots_parking_as_a_gift)

    def test_filter_panoramic_windows(self):
        self.click(self.panoramic_windows)
        time.sleep(10)
        self.open_random_flat()
        self.switch_to_newest_tab()
        self.assert_element_visible(self.lots_panoramic_windows)
        self.switch_to_tab(0)
        self.open_random_flat()
        self.switch_to_newest_tab()
        self.assert_element_visible(self.lots_panoramic_windows)
        self.switch_to_tab(0)
        self.open_random_flat()
        self.switch_to_newest_tab()
        self.assert_element_visible(self.lots_panoramic_windows)
        self.switch_to_tab(0)
        self.open_random_flat()
        self.switch_to_newest_tab()
        self.assert_element_visible(self.lots_panoramic_windows)
        self.switch_to_tab(0)
        self.open_random_flat()
        self.switch_to_newest_tab()
        self.assert_element_visible(self.lots_panoramic_windows)

    def test_filter_high_ceilings(self):
        self.click(self.high_ceilings)
        time.sleep(10)
        self.open_random_flat()
        self.switch_to_newest_tab()
        self.assert_element_visible(self.lots_high_ceilings)
        self.switch_to_tab(0)
        self.open_random_flat()
        self.switch_to_newest_tab()
        self.assert_element_visible(self.lots_high_ceilings)
        self.switch_to_tab(0)
        self.open_random_flat()
        self.switch_to_newest_tab()
        self.assert_element_visible(self.lots_high_ceilings)
        self.switch_to_tab(0)
        self.open_random_flat()
        self.switch_to_newest_tab()
        self.assert_element_visible(self.lots_high_ceilings)
        self.switch_to_tab(0)
        self.open_random_flat()
        self.switch_to_newest_tab()
        self.assert_element_visible(self.lots_high_ceilings)
        self.switch_to_tab(0)
        self.open_random_flat()
        self.switch_to_newest_tab()
        self.assert_element_visible(self.lots_high_ceilings)
        self.switch_to_tab(0)
        self.open_random_flat()
        self.switch_to_newest_tab()
        self.assert_element_visible(self.lots_high_ceilings)

    def test_filter_windows_on_two_sides(self):
        self.click(self.windows_on_two_sides)
        time.sleep(10)
        self.open_random_flat()
        self.switch_to_newest_tab()
        self.assert_element_visible(self.lots_windows_on_two_sides)
        self.switch_to_tab(0)
        self.open_random_flat()
        self.switch_to_newest_tab()
        self.assert_element_visible(self.lots_windows_on_two_sides)
        self.switch_to_tab(0)
        self.open_random_flat()
        self.switch_to_newest_tab()
        self.assert_element_visible(self.lots_windows_on_two_sides)
        self.switch_to_tab(0)
        self.open_random_flat()
        self.switch_to_newest_tab()
        self.assert_element_visible(self.lots_windows_on_two_sides)
        self.switch_to_tab(0)
        self.open_random_flat()
        self.switch_to_newest_tab()
        self.assert_element_visible(self.lots_windows_on_two_sides)

    def test_filter_corner_windows(self):
        self.click(self.corner_windows)
        time.sleep(10)
        self.open_random_flat()
        self.switch_to_newest_tab()
        self.assert_element_visible(self.lots_corner_windows)
        self.switch_to_tab(0)
        self.open_random_flat()
        self.switch_to_newest_tab()
        self.assert_element_visible(self.lots_corner_windows)
        self.switch_to_tab(0)
        self.open_random_flat()
        self.switch_to_newest_tab()
        self.assert_element_visible(self.lots_corner_windows)
        self.switch_to_tab(0)
        self.open_random_flat()
        self.switch_to_newest_tab()
        self.assert_element_visible(self.lots_corner_windows)
        self.switch_to_tab(0)
        self.open_random_flat()
        self.switch_to_newest_tab()
        self.assert_element_visible(self.lots_corner_windows)

    def test_filter_two_level(self):
        self.click(self.two_level)
        time.sleep(10)
        self.open_random_flat()
        self.switch_to_newest_tab()
        self.assert_element_visible(self.lots_two_level)
        self.switch_to_tab(0)
        self.open_random_flat()
        self.switch_to_newest_tab()
        self.assert_element_visible(self.lots_two_level)
        self.switch_to_tab(0)
        self.open_random_flat()
        self.switch_to_newest_tab()
        self.assert_element_visible(self.lots_two_level)
        self.switch_to_tab(0)
        self.open_random_flat()
        self.switch_to_newest_tab()
        self.assert_element_visible(self.lots_two_level)
        self.switch_to_tab(0)
        self.open_random_flat()
        self.switch_to_newest_tab()
        self.assert_element_visible(self.lots_two_level)

    def test_filter_smart_house(self):
        self.click(self.smart_house)
        time.sleep(10)
        self.open_random_flat()
        self.switch_to_newest_tab()
        self.assert_element_visible(self.lots_smart_house)
        self.switch_to_tab(0)
        self.open_random_flat()
        self.switch_to_newest_tab()
        self.assert_element_visible(self.lots_smart_house)
        self.switch_to_tab(0)
        self.open_random_flat()
        self.switch_to_newest_tab()
        self.assert_element_visible(self.lots_smart_house)
        self.switch_to_tab(0)
        self.open_random_flat()
        self.switch_to_newest_tab()
        self.assert_element_visible(self.lots_smart_house)
        self.switch_to_tab(0)
        self.open_random_flat()
        self.switch_to_newest_tab()
        self.assert_element_visible(self.lots_smart_house)

    def test_filter_pantry(self):
        self.click(self.pantry)
        time.sleep(10)
        self.open_random_flat()
        self.switch_to_newest_tab()
        self.assert_element_visible(self.lots_pantry)
        self.switch_to_tab(0)
        self.open_random_flat()
        self.switch_to_newest_tab()
        self.assert_element_visible(self.lots_pantry)
        self.switch_to_tab(0)
        self.open_random_flat()
        self.switch_to_newest_tab()
        self.assert_element_visible(self.lots_pantry)
        self.switch_to_tab(0)
        self.open_random_flat()
        self.switch_to_newest_tab()
        self.assert_element_visible(self.lots_pantry)
        self.switch_to_tab(0)
        self.open_random_flat()
        self.switch_to_newest_tab()
        self.assert_element_visible(self.lots_pantry)

    def test_filter_sorting_popularity(self):
        self.click(self.sorting_menu)
        self.click(self.sorting_popularity)
        time.sleep(10)
        quantity = self.get_purchase_quantity(self.cards_purchase_quantity)
        self.assertTrue(int(quantity[0]) >= int(quantity[1]) >= int(quantity[2]))

    def test_filter_sorting_cost_ascending(self):
        self.click(self.sorting_menu)
        self.click(self.sorting_cost_ascending)
        time.sleep(10)
        costs = self.get_cost_cards(self.cards_costs)
        self.assertTrue(int(costs[0]) <= int(costs[1]) <= int(costs[2]))

    def test_filter_sorting_cost_descending(self):
        self.click(self.sorting_menu)
        self.click(self.sorting_cost_descending)
        time.sleep(10)
        costs = self.get_cost_cards(self.cards_costs)
        self.assertTrue(int(costs[0]) >= int(costs[1]) >= int(costs[2]))

    def test_filter_sorting_square_ascending(self):
        self.click(self.sorting_menu)
        self.click(self.sorting_square_ascending)
        time.sleep(10)
        square = self.get_square_cards(self.cards_squares)
        self.assertTrue(float(square[0]) <= float(square[1]) <= float(square[2]))

    def test_filter_sorting_square_descending(self):
        self.click(self.sorting_menu)
        self.click(self.sorting_square_descending)
        time.sleep(10)
        square = self.get_square_cards(self.cards_squares)
        self.assertTrue(float(square[0]) >= float(square[1]) >= float(square[2]))
