import time
import requests

from PageObjects.HomePage import HomePage
from PageObjects.FlatsPage import FlatsPage


class AllSectionsPresentInFooter(HomePage):

    def setUp(self, **kwargs):
        super().setUp()
        self.open_page()

    def test_from_footer_to_about(self):
        self.click(self.about_in_footer)
        time.sleep(5)
        self.assertEqual('https://strana.artw.dev/about/', self.get_current_url())
        r = requests.get(self.get_current_url())
        self.assertEqual(200, r.status_code)

    def test_from_footer_to_action(self):
        self.click(self.action_in_footer)
        time.sleep(5)
        self.assertEqual('https://strana.artw.dev/mass-media/?type=action', self.get_current_url())
        r = requests.get(self.get_current_url())
        self.assertEqual(200, r.status_code)

    def test_from_footer_to_purchase(self):
        self.click(self.purchase_in_footer)
        time.sleep(5)
        self.assertIn('https://strana.artw.dev/purchase', self.get_current_url())
        r = requests.get(self.get_current_url())
        self.assertEqual(200, r.status_code)

    def test_from_footer_to_contacts(self):
        self.click(self.contacts_in_footer)
        time.sleep(5)
        self.assertEqual('https://strana.artw.dev/contacts/', self.get_current_url())
        r = requests.get(self.get_current_url())
        self.assertEqual(200, r.status_code)

    def test_from_footer_to_commercial(self):
        self.click(self.commercial_in_footer)
        time.sleep(5)
        self.assertEqual('https://strana.artw.dev/projects?kind=commercial', self.get_current_url())
        r = requests.get(self.get_current_url())
        self.assertEqual(200, r.status_code)

    def test_from_footer_to_documents(self):
        self.click(self.documents_in_footer)
        time.sleep(5)
        self.assertEqual('https://strana.artw.dev/documents/', self.get_current_url())
        r = requests.get(self.get_current_url())
        self.assertEqual(200, r.status_code)

    def test_from_footer_to_projects(self):
        self.click(self.projects_in_footer)
        time.sleep(5)
        self.assertEqual('https://strana.artw.dev/projects/', self.get_current_url())
        r = requests.get(self.get_current_url())
        self.assertEqual(200, r.status_code)

    def test_from_footer_to_domashniy(self):
        self.click(self.domashniy_in_footer)
        time.sleep(5)
        self.assertEqual('https://strana.artw.dev/projects/domashnyi', self.get_current_url())
        r = requests.get(self.get_current_url())
        self.assertEqual(200, r.status_code)

    def test_from_footer_to_serdce_sibiri(self):
        self.click(self.serdce_sibiri_in_footer)
        time.sleep(5)
        self.assertEqual('https://strana.artw.dev/projects/serdtche-sibiri', self.get_current_url())
        r = requests.get(self.get_current_url())
        self.assertEqual(200, r.status_code)

    def test_from_footer_to_zvezdny(self):
        self.click(self.zvezdny_in_footer)
        time.sleep(5)
        self.assertEqual('https://strana.artw.dev/projects/zvezdniy', self.get_current_url())
        r = requests.get(self.get_current_url())
        self.assertEqual(200, r.status_code)

    def test_from_footer_to_kolumb(self):
        self.click(self.kolumb_in_footer)
        time.sleep(5)
        self.assertEqual('https://strana.artw.dev/projects/columb', self.get_current_url())
        r = requests.get(self.get_current_url())
        self.assertEqual(200, r.status_code)

    def test_from_footer_to_evropeiskiy_bereg(self):
        self.click(self.evropeiskiy_bereg_in_footer)
        time.sleep(5)
        self.assertEqual('https://strana.artw.dev/projects/evropeiskij-bereg', self.get_current_url())
        r = requests.get(self.get_current_url())
        self.assertEqual(200, r.status_code)

    def test_from_footer_to_flats(self):
        self.click(self.flats_in_footer)
        time.sleep(10)
        self.assertEqual('https://strana.artw.dev/flats/', self.get_current_url())
        r = requests.get(self.get_current_url())
        self.assertEqual(200, r.status_code)

    def test_from_footer_to_studio_apartment(self):
        self.click(self.studio_apartment_in_footer)
        time.sleep(10)
        self.assertIn('?rooms=0', self.get_current_url())
        r = requests.get(self.get_current_url())
        self.assertEqual(200, r.status_code)
        self.open_random_flat()
        self.switch_to_newest_tab()
        expected_rooms = "Студия"
        actual_rooms = self.get_text(FlatsPage.lots_rooms_quantity)
        self.assertIn(expected_rooms, actual_rooms)

    def test_from_footer_to_one_room_apartment(self):
        self.click(self.one_room_apartment_in_footer)
        time.sleep(10)
        self.assertEqual('https://strana.artw.dev/flats/?rooms=1', self.get_current_url())
        r = requests.get(self.get_current_url())
        self.assertEqual(200, r.status_code)
        self.open_random_flat()
        self.switch_to_newest_tab()
        expected_rooms = "1-ком."
        actual_rooms = self.get_text(FlatsPage.lots_rooms_quantity)
        self.assertIn(expected_rooms, actual_rooms)

    def test_from_footer_to_two_room_apartment(self):
        self.click(self.two_room_apartment_in_footer)
        time.sleep(10)
        self.assertEqual('https://strana.artw.dev/flats/?rooms=2', self.get_current_url())
        r = requests.get(self.get_current_url())
        self.assertEqual(200, r.status_code)
        self.open_random_flat()
        self.switch_to_newest_tab()
        expected_rooms = "2-ком."
        actual_rooms = self.get_text(FlatsPage.lots_rooms_quantity)
        self.assertIn(expected_rooms, actual_rooms)

    def test_from_footer_to_three_room_apartment(self):
        self.click(self.three_room_apartment_in_footer)
        time.sleep(10)
        self.assertEqual('https://strana.artw.dev/flats/?rooms=3', self.get_current_url())
        r = requests.get(self.get_current_url())
        self.assertEqual(200, r.status_code)
        self.open_random_flat()
        self.switch_to_newest_tab()
        expected_rooms = "3-ком."
        actual_rooms = self.get_text(FlatsPage.lots_rooms_quantity)
        self.assertIn(expected_rooms, actual_rooms)

    def test_from_footer_to_four_room_apartment(self):
        self.click(self.four_room_apartment_in_footer)
        time.sleep(10)
        self.assertEqual('https://strana.artw.dev/flats/?rooms=4', self.get_current_url())
        r = requests.get(self.get_current_url())
        self.assertEqual(200, r.status_code)
        self.open_random_flat()
        self.switch_to_newest_tab()
        expected_rooms = "4-ком."
        actual_rooms = self.get_text(FlatsPage.lots_rooms_quantity)
        self.assertIn(expected_rooms, actual_rooms)

    def test_from_footer_to_purchase_options(self):
        self.click(self.purchase_options_in_footer)
        time.sleep(5)
        self.assertIn('https://strana.artw.dev/purchase/', self.get_current_url())
        r = requests.get(self.get_current_url())
        self.assertEqual(200, r.status_code)

    def test_from_footer_to_maternal_capital(self):
        self.click(self.maternal_capital_in_footer)
        time.sleep(5)
        self.assertEqual('https://strana.artw.dev/purchase-methods/capital', self.get_current_url())
        r = requests.get(self.get_current_url())
        self.assertEqual(200, r.status_code)

    def test_from_footer_to_installment(self):
        self.click(self.installment_in_footer)
        time.sleep(5)
        self.assertEqual('https://strana.artw.dev/purchase-methods/installment/', self.get_current_url())
        r = requests.get(self.get_current_url())
        self.assertEqual(200, r.status_code)

    def test_from_footer_to_certificates(self):
        self.click(self.certificates_in_footer)
        time.sleep(5)
        self.assertEqual('https://strana.artw.dev/purchase-methods/certificates/', self.get_current_url())
        r = requests.get(self.get_current_url())
        self.assertEqual(200, r.status_code)

    def test_from_footer_to_full_payment(self):
        self.click(self.full_payment_in_footer)
        time.sleep(5)
        self.assertEqual('https://strana.artw.dev/purchase-methods/full-payment/', self.get_current_url())
        r = requests.get(self.get_current_url())
        self.assertEqual(200, r.status_code)

    def test_from_footer_to_insurance(self):
        self.click(self.insurance_in_footer)
        time.sleep(5)
        self.assertEqual('https://strana.artw.dev/purchase-methods/insurance/', self.get_current_url())
        r = requests.get(self.get_current_url())
        self.assertEqual(200, r.status_code)

    def test_from_footer_to_about2(self):
        self.click(self.about2_in_footer)
        time.sleep(5)
        self.assertEqual('https://strana.artw.dev/about/', self.get_current_url())
        r = requests.get(self.get_current_url())
        self.assertEqual(200, r.status_code)

    def test_from_footer_to_builders_and_land_owners(self):
        self.click(self.builders_and_land_owners_in_footer)
        time.sleep(5)
        self.switch_to_newest_tab()
        self.assertEqual('https://land.strana.com/', self.get_current_url())
        r = requests.get(self.get_current_url())
        self.assertEqual(403, r.status_code)

    def test_from_footer_to_tender(self):
        self.click(self.tender_in_footer)
        time.sleep(5)
        self.switch_to_newest_tab()
        self.assertEqual('https://tender.strana.com/', self.get_current_url())
        r = requests.get(self.get_current_url())
        self.assertEqual(200, r.status_code)

    def test_from_footer_to_architecture_bureau(self):
        self.click(self.architecture_bureau_in_footer)
        time.sleep(5)
        self.switch_to_newest_tab()
        self.assertEqual('https://list.strana.com/', self.get_current_url())
        r = requests.get(self.get_current_url())
        self.assertEqual(200, r.status_code)

    def test_from_footer_to_ARTW(self):
        self.click(self.ARTW_in_footer)
        time.sleep(5)
        self.assertEqual('https://artw.ru/', self.get_current_url())
        r = requests.get(self.get_current_url())
        self.assertEqual(200, r.status_code)

    def tearDown(self):
        super().tearDown()
