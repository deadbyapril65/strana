import time
import requests

from PageObjects.HomePage import HomePage


class AllSectionsPresentInBurger(HomePage):

    def setUp(self, **kwargs):
        super().setUp()
        self.open_page()

    def test_from_burger_to_projects(self):
        self.click(self.burger)
        time.sleep(1)
        self.click(self.projects_in_burger)
        time.sleep(5)
        self.assertEqual('https://strana.artw.dev/projects/', self.get_current_url())
        r = requests.get(self.get_current_url())
        self.assertEqual(200, r.status_code)

    def test_from_burger_to_domashniy(self):
        self.click(self.burger)
        self.hover_on_element(self.projects_in_burger)
        time.sleep(1)
        self.click(self.domashniy_in_burger)
        time.sleep(5)
        self.assertEqual('https://strana.artw.dev/projects/domashnyi/', self.get_current_url())
        r = requests.get(self.get_current_url())
        self.assertEqual(200, r.status_code)

    def test_from_burger_to_serdce_sibiri(self):
        self.click(self.burger)
        self.hover_on_element(self.projects_in_burger)
        time.sleep(1)
        self.click(self.serdce_sibiri_in_burger)
        time.sleep(5)
        self.assertEqual('https://strana.artw.dev/projects/serdtche-sibiri/', self.get_current_url())
        r = requests.get(self.get_current_url())
        self.assertEqual(200, r.status_code)

    def test_from_burger_to_zvezdny(self):
        self.click(self.burger)
        self.hover_on_element(self.projects_in_burger)
        time.sleep(1)
        self.click(self.zvezdny_in_burger)
        time.sleep(5)
        self.assertEqual('https://strana.artw.dev/projects/zvezdniy/', self.get_current_url())
        r = requests.get(self.get_current_url())
        self.assertEqual(200, r.status_code)

    def test_from_burger_to_kolumb(self):
        self.click(self.burger)
        self.hover_on_element(self.projects_in_burger)
        time.sleep(1)
        self.click(self.kolumb_in_burger)
        time.sleep(5)
        self.assertEqual('https://strana.artw.dev/projects/columb/', self.get_current_url())
        r = requests.get(self.get_current_url())
        self.assertEqual(200, r.status_code)

    def test_from_burger_to_evropeiskiy_bereg(self):
        self.click(self.burger)
        self.hover_on_element(self.projects_in_burger)
        time.sleep(1)
        self.click(self.evropeiskiy_bereg_in_burger)
        time.sleep(5)
        self.assertEqual('https://strana.artw.dev/projects/evropeiskij-bereg/', self.get_current_url())
        r = requests.get(self.get_current_url())
        self.assertEqual(200, r.status_code)

    def test_from_burger_to_flats(self):
        self.click(self.burger)
        self.click(self.flats_in_burger)
        time.sleep(10)
        self.assertEqual('https://strana.artw.dev/flats/', self.get_current_url())
        r = requests.get(self.get_current_url())
        self.assertEqual(200, r.status_code)

    def test_from_burger_to_studio_apartment(self):
        self.click(self.burger)
        self.hover_on_element(self.flats_in_burger)
        time.sleep(1)
        self.click(self.studio_apartment_in_burger)
        time.sleep(5)
        self.assertEqual('https://strana.artw.dev/flats/?rooms=0', self.get_current_url())
        r = requests.get(self.get_current_url())
        self.assertEqual(200, r.status_code)

    def test_from_burger_to_one_room_apartment(self):
        self.click(self.burger)
        self.hover_on_element(self.flats_in_burger)
        time.sleep(1)
        self.click(self.one_room_apartment_in_burger)
        time.sleep(5)
        self.assertEqual('https://strana.artw.dev/flats/?rooms=1', self.get_current_url())
        r = requests.get(self.get_current_url())
        self.assertEqual(200, r.status_code)

    def test_from_burger_to_two_room_apartment(self):
        self.click(self.burger)
        self.hover_on_element(self.flats_in_burger)
        time.sleep(1)
        self.click(self.two_room_apartment_in_burger)
        time.sleep(7)
        self.assertEqual('https://strana.artw.dev/flats/?rooms=2', self.get_current_url())
        r = requests.get(self.get_current_url())
        self.assertEqual(200, r.status_code)

    def test_from_burger_to_three_room_apartment(self):
        self.click(self.burger)
        self.hover_on_element(self.flats_in_burger)
        time.sleep(1)
        self.click(self.three_room_apartment_in_burger)
        time.sleep(7)
        self.assertEqual('https://strana.artw.dev/flats/?rooms=3', self.get_current_url())
        r = requests.get(self.get_current_url())
        self.assertEqual(200, r.status_code)

    def test_from_burger_to_four_room_apartment(self):
        self.click(self.burger)
        self.hover_on_element(self.flats_in_burger)
        time.sleep(1)
        self.click(self.four_room_apartment_in_burger)
        time.sleep(7)
        self.assertEqual('https://strana.artw.dev/flats/?rooms=4', self.get_current_url())
        r = requests.get(self.get_current_url())
        self.assertEqual(200, r.status_code)

    def test_from_burger_to_commercial(self):
        self.click(self.burger)
        self.click(self.commercial_in_burger)
        time.sleep(5)
        self.assertEqual('https://strana.artw.dev/projects?kind=commercial', self.get_current_url())
        r = requests.get(self.get_current_url())
        self.assertEqual(200, r.status_code)

    def test_from_burger_to_commercial_spb(self):
        self.click(self.burger)
        self.hover_on_element(self.commercial_in_burger)
        self.click(self.spb_commercial_in_burger)
        time.sleep(5)
        self.assertEqual('https://strana.artw.dev/projects/?kind=commercial&city=Q2l0eVR5cGU6Mw==',
                         self.get_current_url())
        r = requests.get(self.get_current_url())
        self.assertEqual(200, r.status_code)

    def test_from_burger_to_commercial_tmn(self):
        self.click(self.burger)
        self.hover_on_element(self.commercial_in_burger)
        self.click(self.tmn_commercial_in_burger)
        time.sleep(5)
        self.assertEqual('https://strana.artw.dev/projects/?kind=commercial&city=Q2l0eVR5cGU6Mg==',
                         self.get_current_url())
        r = requests.get(self.get_current_url())
        self.assertEqual(200, r.status_code)

    def test_from_burger_to_purchase(self):
        self.click(self.burger)
        self.click(self.purchase_in_burger)
        time.sleep(5)
        self.assertIn('https://strana.artw.dev/purchase', self.get_current_url())
        r = requests.get(self.get_current_url())
        self.assertEqual(200, r.status_code)

    def test_from_burger_to_massmedia(self):
        self.click(self.burger)
        self.click(self.massmedia_in_burger)
        time.sleep(5)
        self.assertEqual('https://strana.artw.dev/mass-media?type=action', self.get_current_url())
        r = requests.get(self.get_current_url())
        self.assertEqual(200, r.status_code)

    def test_from_burger_to_news(self):
        self.click(self.burger)
        self.click(self.news_in_burger)
        time.sleep(5)
        self.assertEqual('https://strana.artw.dev/mass-media?type=news', self.get_current_url())
        r = requests.get(self.get_current_url())
        self.assertEqual(200, r.status_code)

    def test_from_burger_to_progress(self):
        self.click(self.burger)
        self.click(self.progress_in_burger)
        time.sleep(5)
        self.assertEqual('https://strana.artw.dev/mass-media?type=progress', self.get_current_url())
        r = requests.get(self.get_current_url())
        self.assertEqual(200, r.status_code)

    def test_from_burger_to_about(self):
        self.click(self.burger)
        self.click(self.about_in_burger)
        time.sleep(5)
        self.assertEqual('https://strana.artw.dev/about/', self.get_current_url())
        r = requests.get(self.get_current_url())
        self.assertEqual(200, r.status_code)

    def test_from_burger_to_purchase_methods(self):
        self.click(self.burger)
        self.click(self.purchase_methods_in_burger)
        time.sleep(5)
        self.assertIn('https://strana.artw.dev/purchase-methods/', self.get_current_url())
        r = requests.get(self.get_current_url())
        self.assertEqual(200, r.status_code)

    def test_from_burger_to_documents(self):
        self.click(self.burger)
        self.click(self.documents_in_burger)
        time.sleep(5)
        self.assertEqual('https://strana.artw.dev/documents/', self.get_current_url())
        r = requests.get(self.get_current_url())
        self.assertEqual(200, r.status_code)

    def test_from_burger_to_investors(self):
        self.click(self.burger)
        self.click(self.investors_in_burger)
        time.sleep(5)
        self.assertEqual('https://strana.artw.dev/investors/', self.get_current_url())
        r = requests.get(self.get_current_url())
        self.assertEqual(200, r.status_code)

    def test_from_burger_to_partners(self):
        self.click(self.burger)
        self.click(self.partners_in_burger)
        time.sleep(5)
        self.assertEqual('https://strana.artw.dev/about/partners/', self.get_current_url())
        r = requests.get(self.get_current_url())
        self.assertEqual(200, r.status_code)

    def test_from_burger_to_tender(self):
        self.click(self.burger)
        self.click(self.tender_in_burger)
        time.sleep(5)
        self.switch_to_newest_tab()
        self.assertEqual('https://tender.strana.com/', self.get_current_url())
        r = requests.get(self.get_current_url())
        self.assertEqual(200, r.status_code)

    def test_from_burger_to_architecture_bureau(self):
        self.click(self.burger)
        self.click(self.architecture_bureau_in_burger)
        time.sleep(5)
        self.switch_to_newest_tab()
        self.assertEqual('https://list.strana.com/', self.get_current_url())
        r = requests.get(self.get_current_url())
        self.assertEqual(403, r.status_code)

    def test_from_burger_to_contacts(self):
        self.click(self.burger)
        self.click(self.contacts_in_burger)
        time.sleep(5)
        self.assertEqual('https://strana.artw.dev/contacts/', self.get_current_url())
        r = requests.get(self.get_current_url())
        self.assertEqual(200, r.status_code)

    def test_from_burger_to_favorites(self):
        self.click(self.burger)
        self.click(self.favorites_in_burger)
        time.sleep(5)
        self.assertEqual('https://strana.artw.dev/favorites/', self.get_current_url())
        r = requests.get(self.get_current_url())
        self.assertEqual(200, r.status_code)

    def test_from_burger_to_land_offer(self):
        self.click(self.burger)
        self.click(self.land_offer_in_burger)
        self.switch_to_newest_tab()
        time.sleep(5)
        self.assertEqual('https://land.strana.com/', self.get_current_url())
        r = requests.get(self.get_current_url())
        self.assertEqual(403, r.status_code)

    def test_from_burger_to_vkontakte(self):
        self.click(self.burger)
        self.click(self.vkontakte_in_burger)
        time.sleep(5)
        self.switch_to_newest_tab()
        self.assertEqual('https://vk.com/strana_com', self.get_current_url())
        r = requests.get(self.get_current_url())
        self.assertEqual(200, r.status_code)

    def test_from_burger_to_youtube(self):
        self.click(self.burger)
        self.click(self.youtube_in_burger)
        time.sleep(5)
        self.switch_to_newest_tab()
        self.assertEqual('https://www.youtube.com/c/СТРАНАДевелопмент', self.get_current_url())
        r = requests.get(self.get_current_url())
        self.assertEqual(200, r.status_code)

    def test_from_burger_to_odnoklasniki(self):
        self.click(self.burger)
        self.click(self.odnoklasniki_in_burger)
        time.sleep(5)
        self.switch_to_newest_tab()
        self.assertEqual('https://ok.ru/stranacom', self.get_current_url())
        r = requests.get(self.get_current_url())
        self.assertEqual(200, r.status_code)

    def test_from_burger_to_telegram(self):
        self.click(self.burger)
        self.click(self.telegram_in_burger)
        time.sleep(5)
        self.switch_to_newest_tab()
        self.assertEqual('https://t.me/stranadevelopment', self.get_current_url())
        r = requests.get(self.get_current_url())
        self.assertEqual(200, r.status_code)

    def tearDown(self):
        super().tearDown()
