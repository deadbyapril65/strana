""" Доделать тесты вакансии после исправления форм"""

import os
import time
from TestsData.Generator import generated_file
from TestsData.Generator import generated_incorrect_file
from TestsData.FormsData import FormsData
from PageObjects.HomePage import HomePage
from PageObjects.PurchasePage import PurchasePage
from PageObjects.ProjectsPage import ProjectsPage
from PageObjects.FlatsPage import FlatsPage
from PageObjects.MassmediaPage import MassmediaPage
from PageObjects.AboutPage import AboutPage


class FormsTestsHome(HomePage):

    def setUp(self, **kwargs):
        super().setUp()
        self.open_page()

    def test_excursion_form_in_footer_positive(self):
        self.send_keys(self.excursion_form_name, FormsData.name)
        self.send_keys(self.excursion_form_phonenumber, FormsData.phone_number)
        self.click(self.excursion_form_submit)
        self.assert_text(FormsData.success_message, self.excursion_form_success_message)
        time.sleep(10)

    def test_excursion_form_in_footer_blank_fields(self):
        self.click(self.excursion_form_submit)
        self.assert_text(FormsData.blank_name_field_message, self.excursion_form_wrong_name_message)
        self.assert_text(FormsData.blank_phone_field_message, self.excursion_form_wrong_phone_message)

    def test_excursion_form_in_footer_negative(self):
        self.send_keys(self.excursion_form_name, FormsData.incorrect_name)
        self.send_keys(self.excursion_form_phonenumber, FormsData.incorrect_phone_number)
        self.click(self.excursion_form_submit)
        self.assert_text(FormsData.wrong_name_message, self.excursion_form_wrong_name_message)
        self.assert_text(FormsData.wrong_phone_message, self.excursion_form_wrong_phone_message)

    def test_subscribing_form_positive(self):
        self.click(self.subscribing_button)
        self.send_keys(self.subscribing_name, FormsData.name)
        self.send_keys(self.subscribing_email, FormsData.email)
        self.click(self.subscribing_submit_button)
        self.assert_text(FormsData.success_message, self.subscribing_success_message)

    def test_subscribing_from_blank_fields(self):
        self.click(self.subscribing_button)
        self.click(self.subscribing_submit_button)
        self.assert_text(FormsData.blank_name_field_message, self.subscribing_wrong_name_message)
        self.assert_text(FormsData.blank_email_field_message, self.subscribing_wrong_email_message)

    def test_subscribing_form_negative(self):
        self.click(self.subscribing_button)
        self.send_keys(self.subscribing_name, FormsData.incorrect_name)
        self.send_keys(self.subscribing_email, FormsData.incorrect_email)
        self.click(self.subscribing_submit_button)
        self.assert_text(FormsData.wrong_name_message, self.subscribing_wrong_name_message)
        self.assert_text(FormsData.wrong_email_field_message, self.subscribing_wrong_email_message)

    def tearDown(self):
        super().tearDown()


class FormsTestsPurchase(PurchasePage):

    def setUp(self, **kwargs):
        super().setUp()
        self.open_page()

    def test_purchase_help_form_positive(self):
        self.click(self.left_application_button)
        self.send_keys(self.purchase_help_form_name, FormsData.name)
        self.send_keys(self.purchase_help_form_phone, FormsData.phone_number)
        self.click(self.purchase_help_form_submit_button)
        self.assert_text(FormsData.success_message, self.purchase_help_form_success_message)

    def test_purchase_help_form_blank_fields(self):
        self.click(self.left_application_button)
        self.click(self.purchase_help_form_submit_button)
        self.assert_text(FormsData.blank_name_field_message, self.purchase_help_form_wrong_name_message)
        self.assert_text(FormsData.blank_phone_field_message, self.purchase_help_form_wrong_phone_message)

    def test_excursion_form_in_footer_negative(self):
        self.click(self.left_application_button)
        self.send_keys(self.purchase_help_form_name, FormsData.incorrect_name)
        self.send_keys(self.purchase_help_form_phone, FormsData.incorrect_phone_number)
        self.click(self.purchase_help_form_submit_button)
        self.assert_text(FormsData.wrong_name_message, self.purchase_help_form_wrong_name_message)
        self.assert_text(FormsData.wrong_phone_message, self.purchase_help_form_wrong_phone_message)

    def tearDown(self):
        super().tearDown()


class FormsTestsProjects(ProjectsPage):

    def setUp(self, **kwargs):
        super().setUp()
        self.open_page()

    def test_callback_form_positive(self):
        self.open_random_project()
        self.click(self.left_application_form_callback)
        self.send_keys(self.callback_form_name, FormsData.name)
        self.send_keys(self.callback_form_phone, FormsData.phone_number)
        self.click(self.callback_from_submit_button)
        self.assert_text(FormsData.success_message, self.callback_form_success_message)

    def test_callback_form_blank_fields(self):
        self.open_random_project()
        self.click(self.left_application_form_callback)
        self.click(self.callback_from_submit_button)
        self.assert_text(FormsData.blank_name_field_message, self.callback_form_wrong_name_message)
        self.assert_text(FormsData.blank_phone_field_message, self.callback_form_wrong_phone_message)

    def test_callback_form_negative(self):
        self.open_random_project()
        self.click(self.left_application_form_callback)
        self.send_keys(self.callback_form_name, FormsData.incorrect_name)
        self.send_keys(self.callback_form_phone, FormsData.incorrect_phone_number)
        self.click(self.callback_from_submit_button)
        self.assert_text(FormsData.wrong_name_message, self.callback_form_wrong_name_message)
        self.assert_text(FormsData.wrong_phone_message, self.callback_form_wrong_phone_message)

    def test_meeting_form_positive(self):
        self.open_random_project()
        time.sleep(5)
        self.execute_script("window.scrollTo(0,11000)")
        self.click(self.left_application_for_meeting)
        self.send_keys(self.meeting_form_name, FormsData.name)
        self.send_keys(self.meeting_form_phone, FormsData.phone_number)
        self.click(self.meeting_form_submit_button)
        self.assert_text(FormsData.success_message, self.meeting_form_success_message)

    def test_meeting_form_blank_fields(self):
        self.open_random_project()
        time.sleep(5)
        self.execute_script("window.scrollTo(0,11000)")
        self.click(self.left_application_for_meeting)
        self.click(self.meeting_form_submit_button)
        self.assert_text(FormsData.blank_name_field_message, self.meeting_form_wrong_name_message)
        self.assert_text(FormsData.blank_phone_field_message, self.meeting_form_wrong_phone_message)

    def test_meeting_form_negative(self):
        self.open_random_project()
        time.sleep(5)
        self.execute_script("window.scrollTo(0,11000)")
        self.click(self.left_application_for_meeting)
        self.send_keys(self.meeting_form_name, FormsData.incorrect_name)
        self.send_keys(self.meeting_form_phone, FormsData.incorrect_phone_number)
        self.click(self.meeting_form_submit_button)
        self.assert_text(FormsData.wrong_name_message, self.meeting_form_wrong_name_message)
        self.assert_text(FormsData.wrong_phone_message, self.meeting_form_wrong_phone_message)

    def test_tour_form_positive(self):
        self.open("https://strana.artw.dev/projects/domashnyi/")
        self.send_keys(self.tour_form_name, FormsData.name)
        self.send_keys(self.tour_form_phone, FormsData.phone_number)
        self.click(self.application_menu_button)
        self.click(self.left_application_for_tour)
        self.click(self.tour_form_submit_button)
        self.assert_text(FormsData.success_message, self.tour_form_success_message)

    def test_tour_form_blank_fields(self):
        self.open("https://strana.artw.dev/projects/domashnyi/")
        self.click(self.application_menu_button)
        self.click(self.left_application_for_tour)
        self.click(self.tour_form_submit_button)
        self.assert_text(FormsData.blank_name_field_message, self.tour_form_wrong_name_message)
        self.assert_text(FormsData.blank_phone_field_message, self.tour_form_wrong_phone_message)

    def test_tour_form_negative(self):
        self.open("https://strana.artw.dev/projects/domashnyi/")
        self.send_keys(self.tour_form_name, FormsData.incorrect_name)
        self.send_keys(self.tour_form_phone, FormsData.incorrect_phone_number)
        self.click(self.application_menu_button)
        self.click(self.left_application_for_tour)
        self.click(self.tour_form_submit_button)
        self.assert_text(FormsData.wrong_name_message, self.tour_form_wrong_name_message)
        self.assert_text(FormsData.wrong_phone_message, self.tour_form_wrong_phone_message)

    def tearDown(self):
        super().tearDown()


class FormsTestsFlats(FlatsPage):

    def setUp(self, **kwargs):
        super().setUp()
        self.open_page()

    def test_purchase_help_form_positive(self):
        self.send_keys(self.purchase_help_form_phone, FormsData.phone_number)
        self.click(self.purchase_help_form_submit_button)
        self.assert_text(FormsData.success_message, self.purchase_help_form_success_message)

    def test_purchase_help_form_blank_fields(self):
        self.click(self.purchase_help_form_submit_button)
        self.assert_text("Обязательное поле", self.purchase_help_form_wrong_phone_message)

    def test_purchase_help_form_negative(self):
        self.send_keys(self.purchase_help_form_phone, FormsData.incorrect_phone_number)
        self.click(self.purchase_help_form_submit_button)
        self.assert_text(FormsData.wrong_phone_message, self.purchase_help_form_wrong_phone_message)

    def test_callback_form_positive(self):
        self.open_random_flat()
        self.switch_to_newest_tab()
        self.click(self.callback_form_button)
        self.send_keys(self.callback_form_name, FormsData.name)
        self.send_keys(self.callback_form_phone, FormsData.phone_number)
        self.click(self.callback_submit_button)
        self.assert_text(FormsData.success_message, self.callback_form_success_message)

    def test_callback_form_blank_fields(self):
        self.open_random_flat()
        self.switch_to_newest_tab()
        self.click(self.callback_form_button)
        self.click(self.callback_submit_button)
        self.assert_text(FormsData.blank_name_field_message, self.callback_form_wrong_name_message)
        self.assert_text(FormsData.blank_phone_field_message, self.callback_form_wrong_phone_message)

    def test_callback_form_negative(self):
        self.open_random_flat()
        self.switch_to_newest_tab()
        self.click(self.callback_form_button)
        self.send_keys(self.callback_form_name, FormsData.incorrect_name)
        self.send_keys(self.callback_form_phone, FormsData.incorrect_phone_number)
        self.click(self.callback_submit_button)
        self.assert_text(FormsData.wrong_name_message, self.callback_form_wrong_name_message)
        self.assert_text(FormsData.wrong_phone_message, self.callback_form_wrong_phone_message)

    def tearDown(self):
        super().tearDown()


class FormsTestsMassmedia(MassmediaPage):

    def setUp(self, **kwargs):
        super().setUp()
        self.open_page()

    def test_mailing_form_positive(self):
        self.send_keys(self.mailing_form_email, FormsData.email)
        self.click(self.mailing_form_submit_button)
        self.assert_text(FormsData.success_message, self.mailing_form_success_message)

    def test_mailing_form_blank_fields(self):
        self.click(self.mailing_form_submit_button)
        self.assert_text(FormsData.blank_email_field_message, self.mailing_form_wrong_email_message)

    def test_mailing_form_negative(self):
        self.send_keys(self.mailing_form_email, FormsData.incorrect_email)
        self.click(self.mailing_form_submit_button)
        self.assert_text(FormsData.wrong_email_field_message, self.mailing_form_wrong_email_message)

    def tearDown(self):
        super().tearDown()


class FormsTestAbout(AboutPage):

    def setUp(self, **kwargs):
        super().setUp()
        self.open_page()

    def test_land_offer_form_positive(self):
        absolute_path = generated_file()
        self.click(self.land_offer_button)
        self.send_keys(self.land_offer_name, FormsData.name)
        self.send_keys(self.land_offer_phone, FormsData.phone_number)
        self.send_keys(self.land_offer_email, FormsData.email)
        self.send_keys(self.land_offer_cadastral, FormsData.cadastral_number)
        self.send_keys(self.land_offer_cost, FormsData.lands_cost)
        self.remove_hidden_class(self.attache_file)
        self.send_keys(self.land_offer_documents, absolute_path)
        self.send_keys(self.land_offer_letter, FormsData.text)
        self.click(self.land_offer_submit_button)
        self.assert_text(FormsData.success_message, self.land_offer_success_message)
        os.remove(absolute_path)

    def test_land_offer_form_blank_fields(self):
        self.click(self.land_offer_button)
        self.click(self.land_offer_submit_button)
        self.assert_text(FormsData.blank_name_field_message, self.land_offer_wrong_name_message)
        self.assert_text(FormsData.blank_phone_field_message, self.land_offer_wrong_phone_message)
        self.assert_text(FormsData.blank_email_field_message, self.land_offer_wrong_email_message)
        self.assert_text(FormsData.blank_cadastral_field_message, self.land_offer_wrong_cadastral_message)
        self.assert_text(FormsData.blank_cost_field_message, self.land_offer_wrong_cost_message)
        self.assert_text(FormsData.blank_document_field_message, self.land_offer_wrong_document_message)

    # Нет валидации у полей#
    def test_land_offer_form_negative(self):
        path = generated_incorrect_file()
        self.click(self.land_offer_button)
        self.send_keys(self.land_offer_name, FormsData.incorrect_name)
        self.send_keys(self.land_offer_phone, FormsData.incorrect_phone_number)
        self.send_keys(self.land_offer_email, FormsData.incorrect_email)
        self.send_keys(self.land_offer_cadastral, FormsData.incorrect_cadastral)
        self.remove_hidden_class(self.attache_file)
        self.send_keys(self.land_offer_documents, path)
        self.click(self.land_offer_submit_button)
        self.assert_text(FormsData.wrong_name_message, self.land_offer_wrong_name_message)
        self.assert_text(FormsData.wrong_phone_message, self.land_offer_wrong_phone_message)
        self.assert_text(FormsData.wrong_email_field_message, self.land_offer_wrong_email_message)
        self.assert_text(FormsData.wrong_cadastral_message, self.land_offer_wrong_cadastral_message)
        self.assert_text(FormsData.wrong_document_message, self.land_offer_wrong_document_message)

    def test_letter_to_director_positive(self):
        self.click(self.letter_to_director_button)
        self.send_keys(self.letter_to_director_name, FormsData.name)
        self.send_keys(self.letter_to_director_phone, FormsData.phone_number)
        self.send_keys(self.letter_to_director_email, FormsData.email)
        self.send_keys(self.letter_to_director_text, FormsData.text)
        self.click(self.letter_to_director_submit_button)
        self.assert_text(FormsData.success_message, self.letter_to_director_success_message)

    def test_letter_to_director_blank_fields(self):
        self.click(self.letter_to_director_button)
        self.click(self.letter_to_director_submit_button)
        self.assert_text(FormsData.blank_name_field_message, self.letter_to_director_wrong_name_message)
        self.assert_text(FormsData.blank_phone_field_message, self.letter_to_director_wrong_phone_message)
        self.assert_text(FormsData.blank_email_field_message, self.letter_to_director_wrong_email_message)
        self.assert_text(FormsData.blank_text_field_message, self.letter_to_director_wrong_text_message)

    def test_letter_to_director_negative(self):
        self.click(self.letter_to_director_button)
        self.send_keys(self.letter_to_director_name, FormsData.incorrect_name)
        self.send_keys(self.letter_to_director_phone, FormsData.incorrect_phone_number)
        self.send_keys(self.letter_to_director_email, FormsData.incorrect_email)
        self.send_keys(self.letter_to_director_text, FormsData.text)
        self.click(self.letter_to_director_submit_button)
        self.assert_text(FormsData.wrong_name_message, self.letter_to_director_wrong_name_message)
        self.assert_text(FormsData.wrong_phone_message, self.letter_to_director_wrong_phone_message)
        self.assert_text(FormsData.wrong_email_field_message, self.letter_to_director_wrong_email_message)

    def test_agency_positive(self):
        self.click(self.partners_tab)
        self.click(self.agency_button)
        self.send_keys(self.agency_name, FormsData.name)
        self.send_keys(self.agency_phone, FormsData.phone_number)
        self.send_keys(self.agency_company_name, FormsData.text)
        self.send_keys(self.agency_city, FormsData.text)
        self.click(self.agency_submit_button)
        self.assert_text(FormsData.success_message, self.agency_success_message)

    def test_agency_blank_fields(self):
        self.click(self.partners_tab)
        self.click(self.agency_button)
        self.click(self.agency_submit_button)
        self.assert_text(FormsData.blank_name_field_message, self.agency_wrong_name_message)
        self.assert_text(FormsData.blank_phone_field_message, self.agency_wrong_phone_message)
        self.assert_text(FormsData.blank_company_field_message, self.agency_wrong_company_name_message)
        self.assert_text(FormsData.blank_city_field_message, self.agency_wrong_city_message)

    def test_agency_negative(self):
        self.click(self.partners_tab)
        self.click(self.agency_button)
        self.send_keys(self.agency_name, FormsData.incorrect_name)
        self.send_keys(self.agency_phone, FormsData.incorrect_phone_number)
        self.send_keys(self.agency_company_name, FormsData.incorrect_name)
        self.send_keys(self.agency_city, FormsData.incorrect_name)
        self.click(self.agency_submit_button)
        self.assert_text(FormsData.wrong_name_message, self.agency_wrong_name_message)
        self.assert_text(FormsData.wrong_phone_message, self.agency_wrong_phone_message)
        self.assert_text(FormsData.wrong_company_name_message, self.agency_wrong_company_name_message)
        self.assert_text(FormsData.wrong_city_name_message, self.agency_wrong_city_message)

    def test_land_offer_form_partners_positive(self):
        path = generated_file()
        self.click(self.partners_tab)
        self.click(self.land_offer_button_partners)
        self.send_keys(self.land_offer_name, FormsData.name)
        self.send_keys(self.land_offer_phone, FormsData.phone_number)
        self.send_keys(self.land_offer_email, FormsData.email)
        self.send_keys(self.land_offer_cadastral, FormsData.cadastral_number)
        self.send_keys(self.land_offer_cost, FormsData.lands_cost)
        self.remove_hidden_class(self.attache_file)
        self.send_keys(self.land_offer_documents, path)
        self.send_keys(self.land_offer_letter, FormsData.text)
        self.click(self.land_offer_submit_button)
        self.assert_text(FormsData.success_message, self.land_offer_success_message)
        os.remove(path)

    def test_land_offer_form_partners_blank_fields(self):
        self.click(self.partners_tab)
        self.click(self.land_offer_button_partners)
        self.click(self.land_offer_submit_button)
        self.assert_text(FormsData.blank_name_field_message, self.land_offer_wrong_name_message)
        self.assert_text(FormsData.blank_phone_field_message, self.land_offer_wrong_phone_message)
        self.assert_text(FormsData.blank_email_field_message, self.land_offer_wrong_email_message)
        self.assert_text(FormsData.blank_cadastral_field_message, self.land_offer_wrong_cadastral_message)
        self.assert_text(FormsData.blank_cost_field_message, self.land_offer_wrong_cost_message)
        self.assert_text(FormsData.blank_document_field_message, self.land_offer_wrong_document_message)

    # Нет валидации у полей#
    def test_land_offer_form_partners_negative(self):
        path = generated_incorrect_file()
        self.click(self.partners_tab)
        self.click(self.land_offer_button_partners)
        self.send_keys(self.land_offer_name, FormsData.incorrect_name)
        self.send_keys(self.land_offer_phone, FormsData.incorrect_phone_number)
        self.send_keys(self.land_offer_email, FormsData.incorrect_email)
        self.send_keys(self.land_offer_cadastral, FormsData.incorrect_cadastral)
        self.remove_hidden_class(self.attache_file)
        self.send_keys(self.land_offer_documents, path)
        self.click(self.land_offer_submit_button)
        self.assert_text(FormsData.wrong_name_message, self.land_offer_wrong_name_message)
        self.assert_text(FormsData.wrong_phone_message, self.land_offer_wrong_phone_message)
        self.assert_text(FormsData.wrong_email_field_message, self.land_offer_wrong_email_message)
        self.assert_text(FormsData.wrong_cadastral_message, self.land_offer_wrong_cadastral_message)
        self.assert_text(FormsData.wrong_document_message, self.land_offer_wrong_document_message)
        os.remove(path)

    def test_subcontractors_form_positive(self):
        path = generated_file()
        self.click(self.partners_tab)
        self.click(self.subcontractor_button)
        self.send_keys(self.subcontractor_name, FormsData.name)
        self.send_keys(self.subcontractor_phone, FormsData.phone_number)
        self.send_keys(self.subcontractor_type_of_work, FormsData.text)
        self.remove_hidden_class(self.attache_file)
        self.send_keys(self.subcontractor_offer, path)
        self.send_keys(self.subcontractor_tell_about, FormsData.text)
        self.click(self.subcontractor_submit_button)
        self.assert_text(FormsData.success_message, self.subcontractor_success_message)
        os.remove(path)

    def test_subcontractors_form_blank_fields(self):
        self.click(self.partners_tab)
        self.click(self.subcontractor_button)
        self.click(self.subcontractor_submit_button)
        self.assert_text(FormsData.blank_name_field_message, self.subcontractor_wrong_name_message)
        self.assert_text(FormsData.blank_phone_field_message, self.subcontractor_wrong_phone_message)
        self.assert_text(FormsData.blank_type_of_work_field_message, self.subcontractor_wrong_type_of_work_message)
        self.assert_text(FormsData.blank_offer_field_message, self.subcontractor_wrong_offer_message)
        self.assert_text(FormsData.blank_tell_about_field_message, self.subcontractor_wrong_tell_about_message)

    def test_subcontractors_form_negative(self):
        path = generated_incorrect_file()
        self.click(self.partners_tab)
        self.click(self.subcontractor_button)
        self.send_keys(self.subcontractor_name, FormsData.incorrect_name)
        self.send_keys(self.subcontractor_phone, FormsData.incorrect_phone_number)
        self.send_keys(self.subcontractor_type_of_work, FormsData.incorrect_name)
        self.remove_hidden_class(self.attache_file)
        self.send_keys(self.subcontractor_offer, path)
        self.send_keys(self.subcontractor_tell_about, FormsData.incorrect_name)
        self.click(self.subcontractor_submit_button)
        self.assert_text(FormsData.wrong_name_message, self.subcontractor_wrong_name_message)
        self.assert_text(FormsData.wrong_phone_message, self.subcontractor_wrong_phone_message)
        self.assert_text(FormsData.wrong_type_of_work_message, self.subcontractor_wrong_type_of_work_message)
        self.assert_text(FormsData.wrong_document_message, self.subcontractor_wrong_offer_message)
        self.assert_text(FormsData.wrong_tell_about_message, self.subcontractor_wrong_tell_about_message)
        os.remove(path)

    def test_massmedia_form_positive(self):
        self.click(self.partners_tab)
        self.click(self.massmedia_button)
        self.send_keys(self.massmedia_name, FormsData.name)
        self.send_keys(self.massmedia_phone, FormsData.phone_number)
        self.send_keys(self.massmedia_email, FormsData.email)
        self.send_keys(self.massmedia_company_name, FormsData.text)
        self.send_keys(self.massmedia_text, FormsData.text)
        self.click(self.massmedia_submit_button)
        self.assert_text(FormsData.success_message, self.massmedia_success_message)

    def test_massmedia_form_blank_fields(self):
        self.click(self.partners_tab)
        self.click(self.massmedia_button)
        self.click(self.massmedia_submit_button)
        self.assert_text(FormsData.blank_name_field_message, self.massmedia_wrong_name_message)
        self.assert_text(FormsData.blank_phone_field_message, self.massmedia_wrong_phone_message)
        self.assert_text(FormsData.blank_email_field_message, self.massmedia_wrong_email_message)
        self.assert_text(FormsData.blank_message_field, self.massmedia_blank_text_message)

    # Нет валидации у поля "Название СМИ"#
    def test_massmedia_form_negative(self):
        self.click(self.partners_tab)
        self.click(self.massmedia_button)
        self.send_keys(self.massmedia_name, FormsData.incorrect_name)
        self.send_keys(self.massmedia_phone, FormsData.incorrect_phone_number)
        self.send_keys(self.massmedia_email, FormsData.incorrect_email)
        self.send_keys(self.massmedia_company_name, FormsData.incorrect_name)
        self.send_keys(self.massmedia_text, FormsData.incorrect_name)
        self.click(self.massmedia_submit_button)
        self.assert_text(FormsData.wrong_name_message, self.massmedia_wrong_name_message)
        self.assert_text(FormsData.wrong_phone_message, self.massmedia_wrong_phone_message)
        self.assert_text(FormsData.wrong_email_field_message, self.massmedia_wrong_email_message)
        self.assert_text(FormsData.wrong_company_name_message, self.massmedia_wrong_company_name_message)
        self.assert_text(FormsData.wrong_message, self.massmedia_wrong_text_message)

    def test_vacancy_form_positive(self):
        path = generated_file()
        self.click(self.vacancy_tab)
        self.execute_script("window.scrollTo(0,1000)")
        self.click(self.vacancy_send_CV_button)
        self.send_keys(self.vacancy_name, FormsData.name)
        self.send_keys(self.vacancy_phone, FormsData.phone_number)
        self.send_keys(self.vacancy_position, FormsData.text)
        self.send_keys(self.vacancy_city, FormsData.text)
        self.remove_hidden_class(self.attache_file)
        self.send_keys(self.vacancy_CV, path)
        self.click(self.vacancy_graduate_checkbox)
        self.send_keys(self.vacancy_graduate_date, "07072012")
        self.send_keys(self.vacancy_speciality, FormsData.text)
        self.send_keys(self.vacancy_institute, FormsData.text)
        self.send_keys(self.vacancy_covering_letter, FormsData.text)
        self.click(self.vacancy_submit_button)
        self.assert_text(FormsData.success_message, self.vacancy_success_message)
        os.remove(path)

    # Не хватает проверок
    def test_vacancy_form_blank_fields(self):
        self.click(self.vacancy_tab)
        self.execute_script("window.scrollTo(0,1000)")
        self.click(self.vacancy_send_CV_button)
        self.click(self.vacancy_submit_button)
        self.assert_text(FormsData.blank_name_field_message, self.vacancy_wrong_name_message)
        self.assert_text(FormsData.blank_phone_field_message, self.vacancy_wrong_phone_message)

    # Не хватает проверок
    def test_vacancy_form_negative(self):
        path = generated_incorrect_file()
        self.click(self.vacancy_tab)
        self.execute_script("window.scrollTo(0,1000)")
        self.click(self.vacancy_send_CV_button)
        self.send_keys(self.vacancy_name, FormsData.incorrect_name)
        self.send_keys(self.vacancy_phone, FormsData.incorrect_phone_number)
        self.send_keys(self.vacancy_position, FormsData.incorrect_name)
        self.send_keys(self.vacancy_city, FormsData.incorrect_name)
        self.remove_hidden_class(self.attache_file)
        self.send_keys(self.vacancy_CV, path)
        self.click(self.vacancy_graduate_checkbox)
        self.send_keys(self.vacancy_graduate_date, "07070001")
        self.send_keys(self.vacancy_speciality, FormsData.incorrect_name)
        self.send_keys(self.vacancy_institute, FormsData.incorrect_name)
        self.send_keys(self.vacancy_covering_letter, FormsData.incorrect_name)
        self.click(self.vacancy_submit_button)
        self.assert_text(FormsData.wrong_name_message, self.vacancy_wrong_name_message)
        self.assert_text(FormsData.wrong_phone_message, self.vacancy_wrong_phone_message)
        self.assert_text(FormsData.wrong_document_message, self.vacancy_wrong_document_message)
        self.assert_text(FormsData.wrong_date_message, self.vacancy_wrong_date_message)
        os.remove(path)

    def tearDown(self):
        super().tearDown()
