import os
import random
import string


# def generated_file():
#     path = rf"C:\Users\KAMPUTER\PycharmProjects\Strana2\TestsData\test{random.randint(10, 100)}.txt"
#     file = open(path, "w")
#     file.write(f'Text for test {random.randint(10, 100)}')
#     file.close()
#     return path

def generated_file():
    file_name = f"test{random.randint(10, 100)}"
    path = rf"../TestsData/{file_name}.txt"
    file = open(path, "w")
    file.write(f'Text for test {random.randint(10, 100)}')
    file.close()
    absolute_path = os.path.abspath(r'../TestsData/' + f'{file_name}.txt')

    return absolute_path


def generated_incorrect_file():
    file_name = f"test{random.randint(10, 100)}"
    path = rf"../TestsData/{file_name}.side"
    file = open(path, "w")
    file.write(f'Text for test {random.randint(10, 100)}')
    file.close()
    absolute_path = os.path.abspath(r'../TestsData/' + f'{file_name}.side')

    return absolute_path


def generate_alphanum_random_string():
    text = [random.choice(string.ascii_lowercase + string.digits) for i in
            range(random.randint(20, 200))]
    return ''.join(text)
