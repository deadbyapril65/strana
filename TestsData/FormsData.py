import random
from TestsData.Generator import generate_alphanum_random_string


class FormsData:
    # forms data
    name = "ИванIvanov123artw"
    incorrect_name = "!!!!<<>////"
    phone_number = "9999999999"
    incorrect_phone_number = "999999"
    email = "123artw@mail.ru"
    incorrect_email = "__123artw"
    special_symbols_email = "!!<>"
    cadastral_number = "99:99:9999999:99"
    incorrect_cadastral = "99:99:99"
    lands_cost = str(random.randint(1, 5000000))
    text = generate_alphanum_random_string()

    # forms messages
    success_message = "Успешно"
    wrong_name_message = "Имя содержит некорректные символы"
    blank_name_field_message = "Поле имени не может быть пустым"
    wrong_phone_message = "Телефон введён неверно"
    blank_phone_field_message = "Поле телефона не может быть пустым"
    blank_email_field_message = "Поле эл. почты не может быть пустым"
    forbidden_symbols_email_message = "В эл. почте не должно быть спецсимволов"
    wrong_email_field_message = "Эл. почта введена неверно"
    blank_cadastral_field_message = "Поле номера участка не может быть пустым"
    blank_cost_field_message = "Поле стоимости не может быть пустым"
    blank_document_field_message = "Необходимо прикрепить файл"
    blank_text_field_message = "Поле текста не может быть пустым"
    wrong_cadastral_message = "Убедитесь, что это значение содержит не менее 11 символов"
    wrong_document_message = "Недопустимый формат файла"
    blank_company_field_message = "Поле название агентства недвижимости не может быть пустым"
    blank_city_field_message = "Поле города не может быть пустым"
    wrong_company_name_message = "Поле название агентства содержит некорректные символы"
    wrong_city_name_message = "Поле города содержит некорректные символы"
    blank_type_of_work_field_message = "Поле Виды работ не может быть пустым"
    blank_offer_field_message = "Поле Коммерческое предложение не может быть пустым"
    blank_tell_about_field_message = "Расскажите что-нибудь о Вашей компании"
    wrong_type_of_work_message = "Поле Виды работ содержит некорректные символы"
    wrong_tell_about_message = "Поле о Вашей компании содержит некорректные символы"
    blank_message_field = "Поле сообщения не может быть пустым"
    wrong_message = "Поле сообщение содержит некорректные символы"
    wrong_date_message = "Введите корректную дату"


