from seleniumbase import BaseCase
from selenium import webdriver
from fake_useragent import UserAgent


class MassmediaPage(BaseCase):
    """ LOCATORS """
    mailing_form_email = "article:nth-child(5) > div > div > form > div > div > div > input"
    mailing_form_submit_button = "[class = 'v-button v-button--default v-button--medium btn_tWWsg']"
    mailing_form_success_message = "//span[contains(text(), 'Успешно')]"
    mailing_form_wrong_email_message = "form > div > span"

    def open_page(self):
        self.open('https://strana.artw.dev/mass-media/')

    def get_new_driver(self, *args, **kwargs):
        """ This method overrides get_new_driver() from BaseCase. """
        useragent = UserAgent()
        options = webdriver.ChromeOptions()
        options.add_argument("start-maximized")
        options.add_argument(f"user-agent= {useragent.random}")
        options.add_argument('--disable-blink-features=AutomationControlled')
        options.add_argument("--disable-extensions")
        options.add_experimental_option('useAutomationExtension', False)
        options.add_experimental_option("excludeSwitches", ["enable-automation"])
        return webdriver.Chrome(options=options)
