import random

from seleniumbase import BaseCase
from selenium import webdriver
from fake_useragent import UserAgent


class HomePage(BaseCase):
    """LOCATORS"""
    flats = "[class = 'lot-item card_g6QjS _cards_-z7Uy']"
    # subscribing_form
    subscribing_button = "button[class='v-circle-button v-circle-button--redline v-circle-button--medium has-hover']"
    subscribing_name = "(//input[@class = 'v-input__native'])[3]"
    subscribing_email = "(//input[@class = 'v-input__native'])[4]"
    subscribing_submit_button = "button[class='v-button v-button--default v-button--medium button_+qivv']"
    subscribing_success_message = "//span[contains(text(), 'Успешно')]"
    subscribing_wrong_name_message = "form > div > div:nth-child(1) > span"
    subscribing_wrong_email_message = " form > div > div:nth-child(2) > span"

    # excursion_form
    excursion_form_name = "(//input[@class = 'v-input__native'])[1]"
    excursion_form_phonenumber = "(//input[@class = 'v-input__native'])[2]"
    excursion_form_submit = "//span[contains(text(), 'Записаться')]"
    excursion_form_success_message = "//span[contains(text(), 'Успешно')]"
    excursion_form_wrong_name_message = "(//span[@class = 'FormHint_82W7I'])[1]"
    excursion_form_wrong_phone_message = "(//span[@class = 'FormHint_82W7I'])[2]"

    # locators_in_header
    purchase_in_header = "a[class = 'item_gX8Lx firstMortgage_POFvA']"
    projects_in_header = "//span[contains(text(), 'Проекты')]"
    flats_in_header = "//span[contains(text(), 'Квартиры')]"
    commercial_in_header = "//span[contains(text(), 'Коммерция')]"
    massmedia_in_header = "//span[contains(text(), 'Акции')]"
    about_in_header = "//span[contains(text(), 'О компании')]"
    purchase_methods_in_header = "//span[contains(text(), 'Способы покупки')]"
    vacancy_in_header = "//span[contains(text(), 'Вакансии')]"
    phone_number_in_header = "[class =  'headerLink button2 c-base mgo-number phone_rOGu-']"
    favorites_in_header = "[class = 'btn-reset btn_FPa8J heart_AMcXn']"
    personal_area_in_header = "[class =  'btn-reset btn_FPa8J accountBtn_du0F7']"

    # locators_in_burger
    burger = "[class = 'btn-reset TheHeaderBurger_W8btt']"
    projects_in_burger = "(//a[@class = 'mainLink_sv8rY'])[1]"
    domashniy_in_burger = "//a[contains(@class, 'sublink_pwRUp') and contains(@href, '/projects/domashnyi')]"
    serdce_sibiri_in_burger = "//a[contains(@class, 'sublink_pwRUp') and contains(@href, 'projects/serdtche-sibiri/')]"
    zvezdny_in_burger = "//a[contains(@class, 'sublink_pwRUp') and contains(@href, '/projects/zvezdniy/')]"
    kolumb_in_burger = "//a[contains(@class, 'sublink_pwRUp') and contains(@href, '/projects/columb/')]"
    evropeiskiy_bereg_in_burger = "//a[contains(@class, 'sublink_pwRUp') and contains(@href, '/projects/evropeiskij-bereg/')]"
    flats_in_burger = "(//a[@class = 'mainLink_sv8rY'])[2]"
    studio_apartment_in_burger = "//a[contains(@class, 'sublink_pwRUp') and contains(@href, '/flats/?rooms=0')]"
    one_room_apartment_in_burger = "//a[contains(@class, 'sublink_pwRUp') and contains(@href, '/flats/?rooms=1')]"
    two_room_apartment_in_burger = "//a[contains(@class, 'sublink_pwRUp') and contains(@href, '/flats/?rooms=2')]"
    three_room_apartment_in_burger = "//a[contains(@class, 'sublink_pwRUp') and contains(@href, '/flats/?rooms=3')]"
    four_room_apartment_in_burger = "//a[contains(@class, 'sublink_pwRUp') and contains(@href, '/flats/?rooms=4')]"
    commercial_in_burger = "(//a[@class = 'mainLink_sv8rY'])[3]"
    spb_commercial_in_burger = "//a[contains(@class, 'sublink_pwRUp') and contains(@href, '/projects/?kind=commercial&city=Q2l0eVR5cGU6Mw%3D%3D')]"
    tmn_commercial_in_burger = "//a[contains(@class, 'sublink_pwRUp') and contains(@href, '/projects/?kind=commercial&city=Q2l0eVR5cGU6Mg%3D%3D')]"
    purchase_in_burger = "//span[contains(@class, 'animationWrapper_54Okv') and contains(text(), 'Ипотека')]"
    massmedia_in_burger = "//span[contains(@class, 'animationWrapper_54Okv') and contains(text(), 'Акции')]"
    news_in_burger = "//span[contains(@class, 'animationWrapper_54Okv') and contains(text(), 'Новости')]"
    progress_in_burger = "//span[contains(@class, 'animationWrapper_54Okv') and contains(text(), 'Ход строительства')]"
    about_in_burger = "//span[contains(@class, 'animationWrapper_54Okv') and contains(text(), 'Компания')]"
    purchase_methods_in_burger = "//span[contains(@class, 'animationWrapper_54Okv') and contains(text(), 'Способы покупки')]"
    documents_in_burger = "//span[contains(@class, 'animationWrapper_54Okv') and contains(text(), 'Документы')]"
    investors_in_burger = "//span[contains(@class, 'animationWrapper_54Okv') and contains(text(), 'Инвесторам')]"
    partners_in_burger = "//span[contains(@class, 'animationWrapper_54Okv') and contains(text(), 'Партнерам')]"
    tender_in_burger = "//span[contains(@class, 'animationWrapper_54Okv') and contains(text(), 'Тендеры')]"
    architecture_bureau_in_burger = "//span[contains(@class, 'animationWrapper_54Okv') and contains(text(), 'Архитектурное бюро')]"
    contacts_in_burger = "//span[contains(@class, 'animationWrapper_54Okv') and contains(text(), 'Контакты')]"
    favorites_in_burger = "[class = 'btn-reset btn_VP7A+ _favorite_9jhV9']"
    land_offer_in_burger = "[class = 'sellHomeIcon_dGegJ']"
    vkontakte_in_burger = "//a[contains(@class, 'SocialLink_+BarE') and contains(@href, 'https://vk.com/strana_com')]"
    youtube_in_burger = "//a[contains(@class, 'SocialLink_+BarE') and contains(@href, 'https://www.youtube.com/c/%D0%A1%D0%A2%D0%A0%D0%90%D0%9D%D0%90%D0%94%D0%B5%D0%B2%D0%B5%D0%BB%D0%BE%D0%BF%D0%BC%D0%B5%D0%BD%D1%82/')]"
    odnoklasniki_in_burger = "//a[contains(@class, 'SocialLink_+BarE') and contains(@href, 'https://ok.ru/stranacom')]"
    telegram_in_burger = "//a[contains(@class, 'SocialLink_+BarE') and contains(@href, 'https://t.me/stranadevelopment')]"

    # locators_in_footer
    about_in_footer = "(//a[@href = '/about/'])[2]"
    action_in_footer = "(//a[@href = '/mass-media/?type=action'])[2]"
    purchase_in_footer = "(//a[@href = '/purchase'])[2]"
    contacts_in_footer = "(//a[@href = '/contacts/'])[2]"
    commercial_in_footer = "(//a[@href = '/commercial/'])"
    documents_in_footer = "(//a[@href = '/documents/'])"
    projects_in_footer = "(//a[@href = '/projects/'])"
    domashniy_in_footer = "(//a[@href = '/projects/domashnyi'])"
    serdce_sibiri_in_footer = "(//a[@href = '/projects/serdtche-sibiri'])"
    zvezdny_in_footer = "(//a[@href = '/projects/zvezdniy'])"
    kolumb_in_footer = "(//a[@href = '/projects/columb'])"
    evropeiskiy_bereg_in_footer = "(//a[@href = '/projects/evropeiskij-bereg'])"
    flats_in_footer = "(//a[@href = '/flats/'])"
    studio_apartment_in_footer = "(//a[@href = '/flats/?rooms=0'])"
    one_room_apartment_in_footer = "(//a[@href = '/flats/?rooms=1'])"
    two_room_apartment_in_footer = "(//a[@href = '/flats/?rooms=2'])"
    three_room_apartment_in_footer = "(//a[@href = '/flats/?rooms=3'])"
    four_room_apartment_in_footer = "(//a[@href = '/flats/?rooms=4'])"
    purchase_options_in_footer = "(//a[@href = '/purchase'])[3]"
    maternal_capital_in_footer = "(//a[@href = '/purchase/capital/'])"
    installment_in_footer = "(//a[@href = '/purchase/installment/'])"
    certificates_in_footer = "(//a[@href = '/purchase/certificates/'])"
    full_payment_in_footer = "(//a[@href = '/purchase/full-payment/'])"
    insurance_in_footer = "(//a[@href = '/purchase/insurance/'])"
    about2_in_footer = "//span[contains(text(), 'Компания')][1]"
    builders_and_land_owners_in_footer = "//span[contains(text(), 'Застройщикам и владельцам земли')]"
    tender_in_footer = "//span[contains(text(), 'Тендеры')]"
    architecture_bureau_in_footer = "//span[contains(text(), 'Архитектурное бюро')]"
    ARTW_in_footer = "(//a[@href = 'https://artw.ru/'])"

    def open_page(self):
        self.open('https://strana.artw.dev/')

    def get_new_driver(self, *args, **kwargs):
        """ This method overrides get_new_driver() from BaseCase. """
        useragent = UserAgent()
        options = webdriver.ChromeOptions()
        options.add_argument("start-maximized")
        options.add_argument(f"user-agent= {useragent.random}")
        options.add_argument('--disable-blink-features=AutomationControlled')
        options.add_argument("--disable-extensions")
        options.add_experimental_option('useAutomationExtension', False)
        options.add_experimental_option("excludeSwitches", ["enable-automation"])
        return webdriver.Chrome(options=options)

    def open_random_flat(self):
        elements = self.find_elements(self.flats)
        elements[random.randint(0, len(elements) - 1)].click()
