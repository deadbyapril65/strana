from seleniumbase import BaseCase
from selenium import webdriver
from fake_useragent import UserAgent


class AboutPage(BaseCase):
    """ LOCATORS """
    partners_tab = "//span[contains(text(), 'Партнерам')]"
    vacancy_tab = "//span[contains(text(), 'Вакансии')]"

    # land_offer_form
    land_offer_button = "[class = 'v-button v-button--inverse v-button--large button_+JqTT']"
    land_offer_button_partners = "//span[contains(text(), 'Оставить заявку на продажу земельного участка')]"
    land_offer_name = "(//input[contains(@class, 'v-input__native')])[1]"
    land_offer_phone = "(//input[contains(@class, 'v-input__native')])[2]"
    land_offer_email = "(//input[contains(@class, 'v-input__native')])[3]"
    land_offer_cadastral = "(//input[contains(@class, 'v-input__native')])[4]"
    land_offer_cost = "(//input[contains(@class, 'v-input__native')])[5]"
    land_offer_documents = "//input[@multiple]"
    land_offer_letter = "(//input[contains(@class, 'v-input__native')])[7]"
    land_offer_submit_button = "[class = 'v-button v-button--default v-button--medium btn_zPCjP']"
    land_offer_success_message = "//h2[contains(text(), 'Успешно')]"
    land_offer_wrong_name_message = "(//span[@class = 'FormHint_82W7I error_MGkg1'])[1]"
    land_offer_wrong_phone_message = "(//span[@class = 'FormHint_82W7I error_MGkg1'])[2]"
    land_offer_wrong_email_message = "(//span[@class = 'FormHint_82W7I error_MGkg1'])[3]"
    land_offer_wrong_cadastral_message = "(//span[@class = 'FormHint_82W7I error_MGkg1'])[4]"
    land_offer_wrong_cost_message = "(//span[@class = 'FormHint_82W7I error_MGkg1'])[5]"
    land_offer_wrong_document_message = "(//span[@class = 'FormHint_82W7I error_MGkg1'])[6]"
    attache_file = "input.file_5Z2dF"

    # letter_to_director_form
    letter_to_director_button = "[class = 'v-circle-button v-circle-button--redline v-circle-button--medium has-hover']"
    letter_to_director_submit_button = "[class = 'v-button v-button--default v-button--medium button_z3qCM']"
    letter_to_director_name = "(//input[@class = 'v-input__native'])[1]"
    letter_to_director_phone = "(//input[@class = 'v-input__native'])[2]"
    letter_to_director_email = "(//input[@class = 'v-input__native'])[3]"
    letter_to_director_text = "(//input[@class = 'v-input__native'])[4]"
    letter_to_director_success_message = "//span[contains(text(), 'Успешно')]"
    letter_to_director_wrong_name_message = "(//span[@class = 'FormHint_82W7I'])[1]"
    letter_to_director_wrong_phone_message = "(//span[@class = 'FormHint_82W7I'])[2]"
    letter_to_director_wrong_email_message = "(//span[@class = 'FormHint_82W7I'])[3]"
    letter_to_director_wrong_text_message = "(//span[@class = 'FormHint_82W7I'])[4]"

    # agency_form
    agency_button = "//span[contains(text(), 'Агентства недвижимости')]"
    agency_name = "(//input[@class = 'v-input__native'])[1]"
    agency_phone = "(//input[@class = 'v-input__native'])[2]"
    agency_company_name = "(//input[@class = 'v-input__native'])[3]"
    agency_city = "(//input[@class = 'v-input__native'])[4]"
    agency_submit_button = "[class = 'v-button v-button--default v-button--medium button_HlPVZ']"
    agency_success_message = "//span[contains(text(), 'Успешно')]"
    agency_wrong_name_message = "(//span[@class = 'FormHint_82W7I'])[1]"
    agency_wrong_phone_message = "(//span[@class = 'FormHint_82W7I'])[2]"
    agency_wrong_company_name_message = "(//span[@class = 'FormHint_82W7I'])[3]"
    agency_wrong_city_message = "(//span[@class = 'FormHint_82W7I'])[4]"

    # subcontractors_form
    subcontractor_button = "//span[contains(text(), 'Подрядчики')]"
    subcontractor_name = "(//input[@class = 'v-input__native'])[1]"
    subcontractor_phone = "(//input[@class = 'v-input__native'])[2]"
    subcontractor_type_of_work = "(//input[@class = 'v-input__native'])[3]"
    subcontractor_offer = "//input[@multiple]"
    subcontractor_tell_about = "(//input[@class = 'v-input__native'])[5]"
    subcontractor_submit_button = "[class = 'v-button v-button--default v-button--medium button_N4Y-s']"
    subcontractor_success_message = "//span[contains(text(), 'Успешно')]"
    subcontractor_wrong_name_message = "(//span[@class = 'FormHint_82W7I'])[1]"
    subcontractor_wrong_phone_message = "(//span[@class = 'FormHint_82W7I'])[2]"
    subcontractor_wrong_type_of_work_message = "(//span[@class = 'FormHint_82W7I'])[3]"
    subcontractor_wrong_offer_message = "(//span[@class = 'FormHint_82W7I'])[4]"
    subcontractor_wrong_tell_about_message = "(//span[@class = 'FormHint_82W7I'])[5]"

    # massmedia_form
    massmedia_button = "//span[contains(text(), 'СМИ')]"
    massmedia_name = "(//input[@class = 'v-input__native'])[1]"
    massmedia_phone = "(//input[@class = 'v-input__native'])[2]"
    massmedia_email = "(//input[@class = 'v-input__native'])[3]"
    massmedia_company_name = "(//input[@class = 'v-input__native'])[4]"
    massmedia_text = "(//input[@class = 'v-input__native'])[5]"
    massmedia_submit_button = "[class = 'v-button v-button--default v-button--medium button_h8NDx']"
    massmedia_success_message = "//span[contains(text(), 'Успешно')]"
    massmedia_wrong_name_message = "(//span[@class = 'FormHint_82W7I'])[1]"
    massmedia_wrong_phone_message = "(//span[@class = 'FormHint_82W7I'])[2]"
    massmedia_wrong_email_message = "(//span[@class = 'FormHint_82W7I'])[3]"
    massmedia_wrong_company_name_message = "(//span[@class = 'FormHint_82W7I'])[4]"
    massmedia_wrong_text_message = "(//span[@class = 'FormHint_82W7I'])[5]"
    massmedia_blank_text_message = "//span[contains(text(), 'Поле сообщения не может быть пустым')]"

    # vacancy_form
    vacancy_send_CV_button = "(//button[@class = 'v-button v-button--inverse v-button--medium btn_xOSJx'])[1]"
    vacancy_name = "(//input[@class = 'v-input__native'])[2]"
    vacancy_phone = "(//input[@class = 'v-input__native'])[3]"
    vacancy_position = "(//input[@class = 'v-input__native'])[4]"
    vacancy_city = "(//input[@class = 'v-input__native'])[5]"
    vacancy_CV = "//input[@type = 'file']"
    vacancy_graduate_checkbox = "//span[@class = 'v-checkbox__input']"
    vacancy_graduate_date = "(//input[@class = 'v-input__native'])[7]"
    vacancy_speciality = "(//input[@class = 'v-input__native'])[8]"
    vacancy_institute = "(//input[@class = 'v-input__native'])[9]"
    vacancy_covering_letter = "(//input[@class = 'v-input__native'])[10]"
    vacancy_submit_button = "[class = 'v-button v-button--default v-button--medium btn_hhpAD']"
    vacancy_success_message = "//h2[contains(text(), 'Успешно')]"
    vacancy_wrong_name_message = "(//span[@class = 'FormHint_82W7I error_2T9TA'])[1]"
    vacancy_wrong_phone_message = "(//span[@class = 'FormHint_82W7I error_2T9TA'])[2]"
    vacancy_wrong_document_message = "(//span[@class = 'FormHint_82W7I error_2T9TA'])[3]"
    vacancy_wrong_date_message = "(//span[@class = 'FormHint_82W7I error_2T9TA'])[4]"

    def open_page(self):
        self.open('https://strana.artw.dev/about')

    def get_new_driver(self, *args, **kwargs):
        """ This method overrides get_new_driver() from BaseCase. """
        useragent = UserAgent()
        options = webdriver.ChromeOptions()
        options.add_argument("start-maximized")
        options.add_argument(f"user-agent= {useragent.random}")
        options.add_argument('--disable-blink-features=AutomationControlled')
        options.add_argument("--disable-extensions")
        options.add_experimental_option('useAutomationExtension', False)
        options.add_experimental_option("excludeSwitches", ["enable-automation"])
        return webdriver.Chrome(options=options)

    def remove_hidden_class(self, class_to_remove):
        remove_hidden_class = f"document.querySelector('{class_to_remove}').setAttribute('class','none')"
        self.add_js_code(remove_hidden_class)
