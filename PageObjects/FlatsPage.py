import random
import re

from seleniumbase import BaseCase
from selenium import webdriver
from fake_useragent import UserAgent


class FlatsPage(BaseCase):
    """ LOCATORS """
    # general
    # Блок в котором расположены все квартиры.
    flats = "article[class='lot-item card_g6QjS _cards_-z7Uy']"
    # Стоимость квартиры на карточке
    flats_cost = ".row_34MUm"

    # purchase_help_form
    purchase_help_form_phone = "//input[@class = 'v-input__native']"
    purchase_help_form_submit_button = "[class = 'v-button v-button--default v-button--medium btn_tWWsg']"
    purchase_help_form_success_message = "//span[contains(text(), 'Успешно')]"
    purchase_help_form_wrong_phone_message = "//span[@class = 'FormHint_82W7I']"

    # callback_form
    callback_form_button = "[class = 'v-button lot-callback v-button--default v-button--medium bookBtn_iT1KW']"
    callback_form_name = "(//input)[5]"
    callback_form_phone = "(//input)[6]"
    callback_submit_button = "[class = 'v-button v-button--default v-button--medium button_3ttm5']"
    callback_form_success_message = "//span[contains(text(), 'Успешно')]"
    callback_form_wrong_name_message = "(//span[@class = 'FormHint_82W7I'])[1]"
    callback_form_wrong_phone_message = "(//span[@class = 'FormHint_82W7I'])[2]"

    # filters_options_start#
    # projects_Tumen
    projects_menu = ".container.FlatsFilter_NSjW7.filter_SqIXk > div.container_zplxv > div:nth-child(1) > div.body_CL1wR > div > div > div > div > div"
    projects_domashniy = "span[title*='Домашний']"
    projects_serdce_sibiri = "span[title*='Сердце Сибири']"
    projects_zvezdny = "span[title*='Звездный']"
    projects_kolumb = "span[title*='Колумб']"
    projects_evropeiskiy_bereg = "span[title*='Европейский берег']"

    # rooms_quantity
    rooms_quantity_studio = "(//button[@class[contains(.,'btn-reset btn_FS9KZ')]])[1]"
    rooms_quantity_one = "(//button[@class[contains(.,'btn-reset btn_FS9KZ')]])[2]"
    rooms_quantity_two = "(//button[@class[contains(.,'btn-reset btn_FS9KZ')]])[3]"
    rooms_quantity_three = "(//button[@class[contains(.,'btn-reset btn_FS9KZ')]])[4]"
    rooms_quantity_four_plus = "(//button[@class[contains(.,'btn-reset btn_FS9KZ')]])[5]"

    # square
    square_from = "(//input[@class = 'c-input__native'])[1]"
    square_to = "(//input[@class = 'c-input__native'])[2]"

    # cost
    cost_from = "(//input[@class = 'c-input__native'])[3]"
    cost_to = "(//input[@class = 'c-input__native'])[4]"

    # houses_Tumen
    houses_menu = "(//div[@class = 'v-select__value'])[4]"
    houses_dom_sibirskiy = "span[title*='Дом \"Сибирский\"']"
    houses_kvartal_gazovikov = "span[title*='Квартал Газовиков']"
    houses_kvartal_geologov = "span[title*='Кв-л Геологов']"
    houses_kvartal_neftyanikov = "span[title*='Кв-л Нефтяников']"
    houses_kvartal_polyarnikov = "span[title*='Квартал Полярников']"
    houses_dom_neptun = "span[title*='Дом Нептун']"
    houses_dom_uran = "span[title*='Дом «Уран»']"
    houses_dom_cuba = "span[title*='Дом Куба']"
    houses_dom_salvador = "span[title*='Дом Сальвадор']"
    houses_dom_london = "span[title*='Дом «Лондон»']"

    # floor
    floor_from = "(//input[@class = 'c-input__native'])[5]"
    floor_to = "(//input[@class = 'c-input__native'])[6]"
    # terrace
    terrace = "//span[contains(text(), 'Терраса')]"

    # parking_as_a_gift
    parking_as_a_gift = "//span[2][contains(text(), 'Паркинг в подарок')]"

    # panoramic_windows
    panoramic_windows = "//span[contains(text(), 'Панорамные окна')]"

    # high_ceilings
    high_ceilings = "//span[contains(text(), 'Высокие потолки')]"

    # windows_on_two_sides
    windows_on_two_sides = "//span[contains(text(), 'Окна на 2 стороны')]"

    # frontage
    frontage = "//span[contains(text(), 'Палисадник')]"

    # corner_windows
    corner_windows = "//span[contains(text(), 'Угловые окна')]"

    # two_level
    two_level = "//span[contains(text(), 'Двухуровневая')]"

    # smart_house
    smart_house = "//span[contains(text(), 'Умный дом')]"

    # pantry
    pantry = "//span[contains(text(), 'Кладовая')]"

    # filters_options_end#

    # sorting
    sorting_menu = "(//div[@class = 'v-select__value'])[6]"
    sorting_popularity = "span[title='по популярности']"
    sorting_cost_ascending = "span[title='по росту стоимости']"
    sorting_cost_descending = "span[title='по убыванию стоимости']"
    sorting_square_ascending = "span[title='по росту площади']"
    sorting_square_descending = "span[title='по убыванию площади']"

    # lots_details
    lots_rooms_quantity = "h1[class = 'c-base title_7xflc']"
    lots_project = "(//div[@class = 'cell_am14R _vice_aDFaA']//span)[2]"
    lots_square = "h1[class = 'c-base title_7xflc']"
    lots_cost = "span[class='priceValue_bsFiv']"
    lots_house = "(//div[@class = 'cell_am14R']//span)[4]"
    lots_floor = "span[class='flatRight_v88rv']"
    lots_terrace = "//*[.='Терраса']"
    lots_parking_as_a_gift = "//div[contains(text(), 'Паркинг в подарок')]"
    lots_panoramic_windows = "//div[contains(text(), 'Панорамные окна')]"
    lots_high_ceilings = "//div[contains(text(), 'Высокие потолки')]"
    lots_windows_on_two_sides = "//div[contains(text(), 'Окна на 2 стороны')]"
    lots_frontage = "//div[contains(text(), 'Палисадник')]"
    lots_corner_windows = "//div[contains(text(), 'Угловые окна')]"
    lots_two_level = "//div[contains(text(), 'Двухуровневая')]"
    lots_smart_house = "//div[contains(text(), 'Умный дом')]"
    lots_pantry = "//div[contains(text(), 'Кладовая')]"

    # cards_details
    cards_purchase_quantity = "//p[contains(text(), 'Купили')]"
    cards_costs = "//div[@class = 'row_Zturw']"
    cards_squares = "//h3[@class = 'subtitle1 c-base title_LEF+q']"

    def open_page(self):
        self.open('https://strana.artw.dev/flats')

    def get_new_driver(self, *args, **kwargs):
        """ This method overrides get_new_driver() from BaseCase. """
        useragent = UserAgent()
        options = webdriver.ChromeOptions()
        options.add_argument("start-maximized")
        options.add_argument(f"user-agent= {useragent.random}")
        options.add_argument('--disable-blink-features=AutomationControlled')
        options.add_argument("--disable-extensions")
        options.add_experimental_option('useAutomationExtension', False)
        options.add_experimental_option("excludeSwitches", ["enable-automation"])
        return webdriver.Chrome(options=options)

    def open_random_flat(self):
        elements = self.find_elements(self.flats)
        elements[random.randint(0, len(elements) - 1)].click()

    def get_square(self, locator):
        string = self.get_text(locator)
        string = string[-10:-1]
        x = re.findall('[-+]?[0-9]*[.,]?[0-9]+(?:[eE][-+]?[0-9]+)?', string)
        square = x[0].replace(',', '.')

        return float(square)

    def get_cost_lots(self, locator):
        cost = ""
        string = self.get_text(locator)
        for i in string:
            if i.isdigit():
                cost += i

        return int(cost)

    def get_purchase_quantity(self, locator):
        quantity = []
        elements = self.find_elements(locator)
        for i in elements:
            text = "".join(i.text)
            x = re.findall('[0-9]+', text)
            quantity += x

        return quantity

    def get_cost_cards(self, locator):
        costs = []
        elements = self.find_elements(locator)
        for i in elements:
            text = "".join(i.text).replace(' ', '')
            x = re.findall('[-+]?[0-9]*[.,]?[0-9]+(?:[eE][-+]?[0-9]+)?', text)
            costs += x

        return costs

    def get_square_cards(self, locator):
        squares = []
        elements = self.find_elements(locator)
        for i in elements:
            text = i.text[-10:-1].replace(',', '.')
            x = re.findall('[-+]?[0-9]*[.,]?[0-9]+(?:[eE][-+]?[0-9]+)?', text)
            squares += x

        return squares
