import random


from seleniumbase import BaseCase
from selenium import webdriver
from fake_useragent import UserAgent


class ProjectsPage(BaseCase):
    """ LOCATORS """

    projects = ".card_DKoAk"

    # application_for_callback
    left_application_form_callback = "button[class = 'v-button callback-button v-button--default v-button--medium flatsButton_nl2N+ sliderButton_M2BbL']"
    callback_form_name = "form > div > div:nth-child(1) > div > div > input"
    callback_form_phone = "form > div > div:nth-child(2) > div > div > input"
    callback_from_submit_button = "button[class = 'v-button v-button--default v-button--medium button_3ttm5']"
    callback_form_success_message = "//span[contains(text(), 'Успешно')]"
    callback_form_wrong_name_message = "form > div > div:nth-child(1) > span"
    callback_form_wrong_phone_message = "form > div > div:nth-child(2) > span"

    # application_for_tour
    application_menu_button = "[class = 'v-select v-select--default v-select--medium is-active has-content']"
    left_application_for_tour = "//span[contains(@class, 'v-select-option__text') and contains(text(), 'Оставьте заявку')]"
    tour_form_name = "form > div:nth-child(1) > div > div > input"
    tour_form_phone = "form > div:nth-child(2) > div > div > input"
    tour_form_submit_button = "[class= 'v-button v-button--default v-button--medium btn_aNW-R']"
    tour_form_success_message = "//span[contains(text(), 'Успешно')]"
    tour_form_wrong_name_message = "form > div:nth-child(1) > span"
    tour_form_wrong_phone_message = "form > div:nth-child(2) > span"

    # application_for_meeting
    left_application_for_meeting = "[class = 'v-button v-button--default v-button--medium is-rounded button_HbUJV']"
    meeting_form_name = "form > div > div:nth-child(1) > div > div > input"
    meeting_form_phone = "form > div > div:nth-child(2) > div > div > input"
    meeting_form_submit_button = "[class = 'v-button v-button--default v-button--medium button_qF5tZ']"
    meeting_form_success_message = "//span[contains(text(), 'Успешно')]"
    meeting_form_wrong_name_message = "form > div > div:nth-child(1) > span"
    meeting_form_wrong_phone_message = "form > div > div:nth-child(2) > span"

    def open_page(self):
        self.open('https://strana.artw.dev/projects')

    def get_new_driver(self, *args, **kwargs):
        """ This method overrides get_new_driver() from BaseCase. """
        useragent = UserAgent()
        options = webdriver.ChromeOptions()
        options.add_argument("start-maximized")
        options.add_argument(f"user-agent= {useragent.random}")
        options.add_argument('--disable-blink-features=AutomationControlled')
        options.add_argument("--disable-extensions")
        options.add_experimental_option('useAutomationExtension', False)
        options.add_experimental_option("excludeSwitches", ["enable-automation"])
        return webdriver.Chrome(options=options)

    def open_random_project(self):
        elements = self.find_elements(self.projects)
        elements[random.randint(1, len(elements) - 1)].click()
