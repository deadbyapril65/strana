from seleniumbase import BaseCase
from selenium import webdriver
from fake_useragent import UserAgent


class PurchasePage(BaseCase):
    # locators
    left_application_button = "button[class = 'v-button v-button--milk v-button--medium v-button--hover-pink mainBtn_wyjEo']"
    purchase_help_form_name = "form > div > div:nth-child(1) > div > div > input"
    purchase_help_form_phone = "form > div > div:nth-child(2) > div > div > input"
    purchase_help_form_submit_button = "button[class = 'v-button v-button--default v-button--medium button_2MgQf']"
    purchase_help_form_success_message = "//span[contains(text(), 'Успешно')]"
    purchase_help_form_wrong_name_message = "form > div > div:nth-child(1) > span"
    purchase_help_form_wrong_phone_message = "form > div > div:nth-child(2) > span"

    def open_page(self):
        self.open('https://strana.artw.dev/purchase/')

    def get_new_driver(self, *args, **kwargs):
        """ This method overrides get_new_driver() from BaseCase. """
        useragent = UserAgent()
        options = webdriver.ChromeOptions()
        options.add_argument("start-maximized")
        options.add_argument(f"user-agent= {useragent.random}")
        options.add_argument('--disable-blink-features=AutomationControlled')
        options.add_argument("--disable-extensions")
        options.add_experimental_option('useAutomationExtension', False)
        options.add_experimental_option("excludeSwitches", ["enable-automation"])
        return webdriver.Chrome(options=options)
